<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        $users = array(
            [
                'id' => 1,
                'name' => 'Paul',
                'email' => 'hoodp@mail.gvsu.edu',
                'password' => bcrypt('dreadsa')
            ],
            [
                'id' => 2,
                'name' => 'Not Paul',
                'email' => 'test@aol.com',
                'password' => bcrypt('test')
            ]
        );
        DB::table('users')->insert($users);
    }
}