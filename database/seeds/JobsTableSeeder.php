<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->delete();

        $jobs = array(
            ['u_id' => 1, 'title' => 'title', 'company' => 'company', 'location' => 'location', 'applied' => 1, 'link' => 'link', 'created_at' => new DateTime, 'updated_at' => new DateTime]
        );

        DB::table('jobs')->insert($jobs);
    }
}
