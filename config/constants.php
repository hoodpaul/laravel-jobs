<?php
return [

    // File types.
    "AVATAR_FILE_TYPE" => 1,
    'RESUME_FILE_TYPE' => 2,
    "COVER_LETTER_TYPE_FILE" => 4,
    "OTHER_FILE_TYPE" => 5,

    // File image types.
    "DEFAULT_FILE_IMAGE" => 6
];