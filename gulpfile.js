var elixir = require('laravel-elixir');
require('laravel-elixir-image-optimize');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	var path = function(path) {
		return '../../../public/' + path;
	};

	// Copy font awesome fonts to fonts folder.
	mix.copy('public/bower/font-awesome/fonts', 'public/build/fonts');
	mix.copy('public/bower/bootstrap/fonts', 'public/build/fonts/bootstrap');
	mix.copy('public/bower/multiselect/img/switch.png', 'public/build/img');

	// Compress images.
	mix.imageOptimize('resources/assets/images', 'public/img');

	// Custom sass file.
	mix.sass(path('scss/op37.scss'));

	// Css file for plugins used on all pages.
	mix.styles([
		path('bower/bootstrap/dist/css/bootstrap.css'),
		path('css/bootstrap-reset.css'),
		path('bower/font-awesome/css/font-awesome.css'),
		path('css/slidebars.css'),
		path('bower/toastr/toastr.css')
	], 'public/css/vendor.css');

	// Custom css used on all pages.
	mix.styles([
		path('css/vendor.css'),
		path('css/op37.css'),
		path('css/style.css'),
		path('css/style-responsive.css')
	], 'public/css/all.css');

	// Default js file for all pages.
	mix.scripts([
		path('bower/jquery/dist/jquery.js'),
		path('bower/bootstrap/dist/js/bootstrap.js'),
		path('bower/jquery.nicescroll/dist/jquery.nicescroll.min.js'),
		path('bower/respond/dest/respond.min.js'),
		path('js/slidebars.min.js'),
		path('bower/toastr/toastr.js'),
		path('js/common-scripts.js')
	], 'public/js/all.js');

	// Js file for ie.
	mix.scripts([
		path('js/html5shiv.js'),
		path('bower/respond/dest/respond.min.js')
	], 'public/js/ie.js');

	// Custom css for the jobs page.
	mix.styles([
		path('assets/advanced-datatable/media/css/demo_page.css'),
		path('bower/datatables/media/css/dataTables.bootstrap.css')
	], 'public/css/jobs.css');

	// Custom js for the jobs index page.
	mix.scripts([
		path('bower/datatables/media/js/jquery.dataTables.js'),
		path('bower/datatables/media/js/dataTables.bootstrap.js'),
		path('bower/datatables.net-responsive/js/dataTables.responsive.js'),
		path('bower/jquery.customSelect/jquery.customSelect.min.js'),
		path('js/jobs/index.js'),
		path('js/googlemaps.js')
	], 'public/js/jobs.js');

	// Custom css for the jobs create page.
	mix.styles([
		path('bower/multiselect/css/multi-select.css'),
		path('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css'),
		path('css/jquery.steps.css')
	], 'public/css/jobs-create.css');

	// Custom js for the jobs create page.
	mix.scripts([
		path('bower/multiselect/js/jquery.multi-select.js'),
		path('assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js'),
		path('assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js'),
		path('js/jquery.steps.min.js'),
		path('js/jquery.stepy.js'),
		path('js/jobs/create.js')
	], 'public/js/jobs-create.js');

	// Custom js for the files page.
	mix.scripts([
		path('assets/dropzone/dropzone.min.js'),
		path('bower/datatables/media/js/jquery.dataTables.js'),
		path('bower/datatables/media/js/dataTables.bootstrap.js'),
		path('bower/datatables.net-responsive/js/dataTables.responsive.js'),
		path('js/files/index.js')
	], 'public/js/files.js');

	// Custom css for the files page.
	mix.styles([
		path('assets/advanced-datatable/media/css/demo_page.css'),
		path('bower/datatables/media/css/dataTables.bootstrap.css'),
		path('assets/dropzone/css/dropzone.min.css')
	], 'public/css/files.css');

	// File versions for updating cache.
	mix.version([
		'css/vendor.css', 'css/all.css', 'css/jobs-create.css', 'css/files.css',
		'css/jobs.css', 'js/all.js', 'js/ie.js', 'js/jobs.js',
		'js/jobs-create.js', 'js/files.js'
	]);
});
