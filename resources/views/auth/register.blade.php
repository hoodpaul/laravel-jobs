@extends('layouts.app')
@section('content')
    {!! Form::open([
        'class' => 'form-signin',
        'id' => 'register',
        'role' => 'form',
        'route' => ['register']
    ]) !!}
        <h2 class="form-signin-heading">registration</h2>
        <div class="login-wrap">
            <p>Enter your personal details below</p>
            <div class="form-group">
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Full Name', 'autofocus']) !!}
                <p class="help-block">{!! $errors->default->first("name") !!}</p>
            </div>
            <div class="form-group">
                {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                <p class="help-block">{!! $errors->default->first("email") !!}</p>
            </div>
            <div class="form-group">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                <p class="help-block">{!! $errors->default->first("password") !!}</p>
            </div>
            <div class="form-group">
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Re-type password']) !!}
                <p class="help-block"></p>
            </div>
            <div class="form-group">
                {!! Form::checkbox("agree", 1) !!}
                <label class="no-bold" for="agree">I agree to the Terms of Service and Privacy Policy</label>
                <p class="help-block">{!! $errors->default->first("agree") !!}</p>
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-login btn-block']) !!}
            </div>
                <div class="registration">
                    Already Registered?
                    {!! link_to_route('login', 'Login') !!}
                </div>

        </div>
    {!! Form::close() !!}
@endsection
@section('endScripts')
    <script>
        var errors = document.getElementsByClassName("help-block");
        for (var i = 0; i < errors.length; i++) {
            if (errors[i].textContent.length !== 0)
                    errors[i].parentElement.className += " has-error";
        }
    </script>
@endsection