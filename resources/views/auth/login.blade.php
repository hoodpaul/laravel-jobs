@extends('layouts.app')
@section('content')
    @if (session('status'))
        <span id="status" class="hidden">{{ session('status') }}</span>
    @endif
    @if (count($errors))
        <ul id="errors" class="hidden">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {!! Form::open([
        'route' => ['login'],
        'role' => 'form',
        'class' => 'form-signin'
    ]) !!}
    <h2 class="form-signin-heading">Welcome to op37</h2>
    <div class="login-wrap">
        <div class="form-group">
            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'autofocus']) !!}
        </div>
        <div class="form-group">
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
        </div>
        <div class="form-group">
            <div class="pull-left">
                {!! Form::checkbox('remember', 'remember', null, $options = ['id' => 'remember']) !!}
                {!! Form::label('remember', 'Remember me', ['class' => 'no-bold']) !!}
            </div>
            <div class="pull-right">
                <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            {!! Form::submit('Sign in', ['class' => 'btn btn-lg btn-login btn-block']) !!}
        </div>
        <div class="form-group registration">
            Don't have an account yet?
            {!! link_to_route('register', 'Create an account') !!}
        </div>
    </div>
    {!! Form::close() !!}
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Forgot Password?</h4>
                </div>
                {!! Form::open([
                    'route' => 'password.email',
                    'method' => 'POST',
                    'id' => 'forgot'
                ]) !!}
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>
                        {!! Form::email('email', old('email'), ['placeholder' => 'Email', 'class' => 'form-control placeholder-no-fix']) !!}
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('endScripts')
    <script>

        // Get the form elements & add email focus method.
        var forgotForm = function() {

            // Modal that holds the form.
            var modal = $("#myModal");

            // The actual form.
            var form = document.forms["forgot"];

            // All of the form elements.
            var elements = document.forms["forgot"].elements;

            var emailFocus = function() {
                elements["email"].focus();
            };

            return {
                elements: elements,
                emailFocus: emailFocus,
                form: form,
                modal: modal
            };
        }();

        forgotForm.modal.on('shown.bs.modal', function() {

            // Add the focus to the email input.
            forgotForm.emailFocus();
        });

        forgotForm.form.addEventListener("submit", function(e) {
            e.preventDefault();

            // Create ajax request.
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {

                    // Show the proper status message.
                    switch (xhr.status) {
                        case 200:
                            forgotForm.modal.modal('hide');
                            toastr.success(xhr.responseText);
                            break;
                        default:
                            forgotForm.emailFocus();
                            toastr.error(xhr.responseText, null, {
                                preventDuplicates: true
                            });
                            break;
                    }
                }
            };

            // Get the token value & the input email.
            var token = forgotForm.elements["_token"].value;
            var email = forgotForm.elements["email"].value;

            // Build the post url.
            var url = "./password/email?_token=" + token + "&email=" + email;
            xhr.open("POST", url);
            xhr.send();
        });
    </script>
@endsection