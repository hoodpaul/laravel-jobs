@extends('layouts.app')
@section('content')
    @if (count($errors))
        <ul id="errors" class="hidden">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {!! Form::open([
        'route' => 'password.reset',
        'role' => 'form',
        'class' => 'form-signin',
        'method' => 'POST'
    ]) !!}
    <h2 class="form-signin-heading">Reset Password</h2>
    <div class="login-wrap">
        <p>Enter your email new password below</p>
        <div class="form-group">
            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'autofocus']) !!}
        </div>
        <div class="form-group">
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
        </div>
        <div class="form-group">
            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Re-type password']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-login btn-block']) !!}
        </div>
    </div>
    {!! Form::hidden('token', $token) !!}
    {!! Form::close() !!}
@endsection
@section('endScripts')
    <script>
        (function() {

            // Get error messages.
            var errors = document.getElementById("errors");

            //
        }());
    </script>
@endsection