<!DOCTYPE html>
<html>
<head>
    <script>
        var CLIENT_ID = "301431777316-hv21omdo88q30dpdlpbe008jbebkbgbh.apps.g" +
                "oogleusercontent.com";

        var SCOPES = ["https://www.googleapis.com/auth/gmail.readonly"];

        /**
         * Check if current user has authorized this application.
         */
        function checkAuth() {
            gapi.auth.authorize({
                'client_id': CLIENT_ID,
                'scope': SCOPES.join(' '),
                'immediate': true
            }, handleAuthResult);
        }

        /**
         * Handle response from authorization server.
         */
        function handleAuthResult(authResult) {
            var authorizeDiv = document.getElementById("authorize-div");
            if (authResult && !authResult.error) {

                // Hide auth UI, then load client library.
                authorizeDiv.style.display = "none";
                loadGmailApi();
            } else {

                // Show auth UI, allow the user to initiate authorization by
                // clicking authorize button.
                authorizeDiv.style.display = "inline";
            }
        }

        /**
         * Initiate auth flow in response to user clicking authorize button.
         */
        function handleAuthClick(event) {
            gapi.auth.authorize({
                client_id: CLIENT_ID,
                scope: SCOPES,
                immediate: false
            }, handleAuthResult);
        }

        /**
         * Load Gmail API client library. List labels once client library is loaded.
         */
        function loadGmailApi() {
            gapi.client.load('gmail', 'v1', listLabels);
        }

        /**
         * Print all labels in the authorized user's inbox. If no labels
         * are found an appropriate message is printed.
         */
        function listLabels() {
            var request = gapi.client.gmail.users.labels.list({
                'userId': 'me'
            });

            request.execute(function(resp) {
                var labels = resp.labels;
                appendPre('Labels:');

                if (labels && labels.length > 0) {
                    for (var i = 0; i < labels.length; i++) {
                        var label = labels[i];
                        appendPre(label.name);
                    }
                } else {
                    appendPre('No labels found.');
                }
            });
        }

        /**
         * Append a pre element to the body containing the given message as
         * its text node.
         */
        function appendPre(message) {
            var pre = document.getElementById("output");
            var textContent = document.createTextNode(message + "\n");
            pre.appendChild(textContent);
        }

    </script>
    <script src="https://apis.google.com/js/client.js?onload=checkAuth">
    </script>
</head>
<body>
    <div id="authorize-div" style="display: none">
        <span>Authorize acccess to Gmail API</span>

        <!-- Button for the user to click to initiate auth sequence -->
        <button id="authorize-button" onclick="handleAuthClick(event)">
            Authorize
        </button>
    </div>
    <pre id="output"></pre>
</body>
</html>
