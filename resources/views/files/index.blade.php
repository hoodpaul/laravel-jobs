@extends('layouts.app')
@section('headStyles')
    {!! HTML::style(elixir('css/files.css')) !!}
    <meta name="_token" content="{!! csrf_token() !!}">
@endsection
@section('content')
    <div id="hidden-type-button" class="btn-group type">
        <button class="btn btn-xs status-btn" type="button"></button>
        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
        </button>
        <ul role="menu" class="dropdown-menu">
            @foreach ($file_types as $type)
                <li>
                    <a class="update-type" href="#" data-type-id="{!! $type->id !!}">{!! $type->name !!}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <section class="panel">
        <div class="panel-heading">Files</div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="hidden-table-info" class="table table-condensed table-striped display table-advanced table-hover responsive no-wrap">
                    <thead>
                    <th>File Name</th>
                    <th>Date Modified</th>
                    <th class="text-center">Size</th>
                    <th class="text-center">Kind</th>
                    <th class="text-center">Active</th>
                    <th class="text-center">Actions</th>
                    </thead>
                </table>
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading">
            Dropzone File Upload
        </header>
        <div class="panel-body">
            {!! Form::open([
                'route' => 'files.store',
                'id' => 'my-awesome-dropzone',
                'class' => 'dropzone'
            ]) !!}
            {!! Form::close() !!}
        </div>
    </section>
@endsection
@section('endScripts')
    {!! HTML::script(elixir('js/files.js')) !!}
@endsection