@section('headStyles')
    {!! HTML::style('assets/toastr-master/toastr.css') !!}
@endsection
@section('content')
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Search Results
        </header>
        <table id="results" class="table table-striped table-advance table-hover">
            <thead>
            <tr>
                <th>Title</th>
                <th>Company</th>
                <th>Location</th>
                <th>Posted</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody id="result-body">
            @if (!isset($keys))
                @foreach ($data as $job)
                    <tr>
                        <td class="job-title">{{ $job->title }}</td>
                        <td class="job-company">{{ $job->company }}</td>
                        <td class="job-location">{{ $job->location }}</td>
                        <td>{{ date('Y-m-d', strtotime($job->date)) }}</td>
                        <td class="actions">
                            <a data-toggle="tooltip" title="Go to job posting" class="job-link" target="_blank" href="{{ $job->link }}">
                                <i class="fa fa-hand-o-right"></i>
                            </a>
                            <a data-toggle="tooltip" title="Add to my jobs" class="add-job" href="#">
                                <i class="fa fa-plus"></i>
                            </a>
                            <i id="search-minus" data-toggle="tooltip" title="Remove from job search" class="fa fa-minus"></i>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </section>
</div>
@endsection
@section('endScripts')
    {!! HTML::script('assets/toastr-master/toastr.js') !!}
    <script>
        $("[data-toggle='tooltip']").tooltip();

        document.getElementById("result-body").addEventListener('click', function(e) {
            if (e.target.className === "fa fa-plus") {

                // current element to extract data from
                var ele = e.target.parentElement.previousElementSibling;

                // get the link
                var link = encodeURIComponent(ele.getAttribute("href"));

                // go to the previous two cells
                ele = ele.parentElement.previousElementSibling
                        .previousElementSibling;

                // get the job location, company, and title from prev. eles
                var location = ele.textContent;
                var company = ele.previousElementSibling.textContent;
                var title = ele.previousElementSibling.previousElementSibling
                        .textContent;

                // build the post request & replace all of the spaces with %20
                var post = "link=" + link + "&location=" + location +
                        "&company=" + company + "&title=" + title;
                post = post.replace(/ /g, "%20");

                // send the ajax request
                var ajax = new XMLHttpRequest();
                ajax.onreadystatechange = function() {
                    if (ajax.readyState == 4 && ajax.status == 200) {
                        var data = JSON.parse(ajax.response);
                        toastr.info(data.title + " has been added to your jobs" +
                                " list.");
                    }
                };
                ajax.open("POST", "/search/jobs/addJob", true);
                ajax.setRequestHeader("X-CSRF-Token", "{!! csrf_token() !!}");
                ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajax.send(post);
            } else if (e.target.className === "fa fa-minus") {

                // get the table
                var results = document.getElementById("results");

                // get the row index
                var rowIndex = e.target.parentElement.parentElement.rowIndex;

                // remove the row from the table
                results.deleteRow(rowIndex);
            }
            e.stopPropagation();
        });
    </script>
@endsection