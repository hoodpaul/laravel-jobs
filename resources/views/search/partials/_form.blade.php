<div class="panel-heading">Search For {{ $type }}</div>
<div class="panel-body">
    {!! Form::open(['route' => $route, 'method' => 'GET', 'class' => 'form-horizontal', 'role' => 'form']) !!}
    @foreach ($names as $name)
        <div class="form-group">
            {!! Form::label($name, $name, ["class" => "col-sm-2 control-label"]) !!}
            <div class="col-sm-10">
                @if ($name != "jc")
                    {!! Form::text($name, "", ["class" => "form-control"]) !!}
                @else
                    {!! Form::select($name, [
                        '' => '',
                        '1' => 'Accounting / Finance',
                        '2' => 'Administrative',
                        '3' => 'Analyst',
                        '4' => 'Architecture / Drafting',
                        '5' => 'Art / Design / Entertainment',
                        '6' => 'Banking / Loan / Insurance',
                        '7'	=> 'Beauty / Wellness',
                        '8'	=> 'Business Development / Consulting',
                        '9'	=> 'Education',
                        '10' =>	'Engineering (Non-software)',
                        '11' =>	'Facilities / General Labor',
                        '12' =>	'Hospitality',
                        '13' =>	'Human Resources',
                        '14' =>	'Installation / Maintenance / Repair',
                        '15' =>	'Legal',
                        '16' =>	'Manufacturing / Production / Construction',
                        '17' =>	'Marketing / Advertising / PR',
                        '18' =>	'Medical / Healthcare',
                        '19' =>	'Non-Profit / Volunteering',
                        '20' =>	'Product / Project Management',
                        '21' =>	'Real Estate',
                        '22' =>	'Restaurant / Food Services',
                        '23' =>	'Retail',
                        '24' =>	'Sales / Customer Care',
                        '25' =>	'Science / Research',
                        '26' =>	'Security / Law Enforcement',
                        '27' =>	'Senior Management',
                        '28' =>	'Skilled Trade',
                        '29' =>	'Software Development / IT',
                        '30' => 'Sports / Fitness',
                        '31' =>	'Travel / Transportation',
                        '32' =>	'Writing / Editing / Publishing',
                        '33' =>	'Other'
                    ], ["class" => "form-control"]) !!}
                @endif
            </div>
        </div>
    @endforeach
    <div class="col-sm-10 col-sm-offset-2">
        <button {!! $type == "Companies" ? 'onclick="return false"' : '' !!}class="btn btn-info">
            <i class="fa fa-search"></i>
            Search for {{ $type }}
        </button>
    </div>
    {{--{!! Form::hidden('action', $action) !!}
    {!! Form::hidden('returnCities', 'true') !!}
    {!! Form::hidden('returnStates', 'true') !!}
    {!! Form::hidden('returnJobTitles', 'true') !!}
    {!! Form::hidden('returnEmployers', 'true') !!}--}}
    {!! Form::close() !!}
</div>
