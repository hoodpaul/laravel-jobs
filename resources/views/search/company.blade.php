@extends('layouts.app')
@section('content')
    @if (isset($data))
        @include('search.partials._results', [
            'data' => $data,
            'keys' => ['employers']
        ])
    @endif
    <div class="row">
        <div class="col-xs-12">
            <section class="panel">
                @include('search.partials._form', [
                    "route" => "search.companies",
                    /*'names' => [
                        "q", "l", "city", "state", "country", "pn", "ps"
                    ],*/
                    'names' => ["Job Title", "Category", "Location"],
                    "type" => "Companies",
                    "action" => "employers"
                ])
            </section>
        </div>
    </div>
    <a class="pull-right" href='http://www.glassdoor.com/index.htm'>
        powered by <img src='https://www.glassdoor.com/static/img/api/glassdoor_logo_80.png' title='Job Search' />
    </a>
@endsection