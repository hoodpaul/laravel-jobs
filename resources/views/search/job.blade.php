@extends('layouts.app')
@section('content')
    @if (isset($data))
        @include('search.partials._results', [
            'data' => $data
        ])
    @endif
    <div class="row">
        <div class="col-xs-12">
            <section class="panel">
                @include('search.partials._form', [
                    "route" => "search.jobs",
                    /*'names' => [
                        "q", "e", "l", "city", "state", "country", "fromAge", "jobType",
                        "minRating", "radius", "jt", "jc"
                    ],*/
                    'names' => ["Job Title", "Location"],
                    "type" => "Jobs",
                    "action" => "jobs-stats"
                ])
            </section>
        </div>
    </div>
    {{--<a class="pull-right" href='http://www.glassdoor.com/index.htm'>
        powered by <img src='https://www.glassdoor.com/static/img/api/glassdoor_logo_80.png' title='Job Search' />
    </a>
    <span id=indeed_at><a href="http://www.indeed.com/">jobs</a> by <a
                href="http://www.indeed.com/" title="Job Search"><img
                    src="http://www.indeed.com/p/jobsearch.gif" style="border: 0;
vertical-align: middle;" alt="Indeed job search"></a></span>--}}

@endsection
@section('endScripts')
    <script>
        var request = "http://api.indeed.com/ads/apisearch?publisher=4188194085276129&q=java&l=austin%2C+tx&sort=&radius=&st=&jt=&start=&limit=&fromage=&filter=&latlong=1&co=us&chnl=&userip=1.2.3.4&useragent=Mozilla/%2F4.0%28Firefox%29&v=2&format=json&callback=callback";
        function callback(data) {
            console.log(data);
        }
        var script = document.createElement("script");
        script.src = request;
        document.getElementsByTagName("head")[0].appendChild(script);
    </script>
@endsection