<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Coming Soon </title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- coming soon styles -->
    <link href="css/soon.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>

    <!-- Google Analytics -->
    <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'UA-69478712-1', 'auto');
        ga('send', 'pageview');
    </script>
    {!! HTML::script("https://www.google-analytics.com/analytics.js", ['async']) !!}
    <!-- End Google Analytics -->
    <![endif]-->
</head>

<body class="cs-bg">
<!-- START HEADER -->
<section id="header">
    <div class="container">
        <header>
            <!-- HEADLINE -->
            <a class="logo floatless">OP<span>37</span></a>
            <h1 > Beta Coming Soon...</h1>
            <br/>
            {{--<p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos </p>--}}
        </header>
        <!-- START TIMER -->
        <div id="timer" data-animated="FadeIn">
            <p id="message"></p>
            <div id="days" class="timer_box"></div>
            <div id="hours" class="timer_box"></div>
            <div id="minutes" class="timer_box"></div>
            <div id="seconds" class="timer_box"></div>
        </div>
        <!-- END TIMER -->
        <div class="col-lg-4 col-lg-offset-4 mt centered">
            @if (Session::has('message'))
                <h1>{{ Session::get('message') }}</h1>
                {{--<h1>Thank you! We'll let you know when we launch.</h1>--}}
            @else
                <h4>LET ME KNOW WHEN YOU LAUNCH</h4>
                {!! Form::open(['route' => 'soon.store']) !!}
                    <div class="form-group">
                        {!! Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Enter email']) !!}
                    </div>
                    {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            @endif
        </div>

    </div>

</section>
<!-- END HEADER -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/soon/plugins.js"></script>
<script src="js/soon/custom.js"></script>

</body>
<!-- END BODY -->
</html>