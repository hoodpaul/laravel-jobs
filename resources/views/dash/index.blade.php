@extends('layouts.app')
@section('headStyles')
@endsection
@section('content')
    <div class="row state-overview">
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol terques">
                    <i class="fa fa-user"></i>
                </div>
                <div class="value">
                    <h1 class="count">
                        0
                    </h1>
                    <p>Total Students</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol red">
                    <i class="fa fa-briefcase"></i>
                </div>
                <div class="value">
                    <h1 class=" count2">
                        0
                    </h1>
                    <p>Unique Companies</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol yellow">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="value">
                    <h1 class=" count3">
                        0
                    </h1>
                    <p>Applications</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol blue">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="value">
                    <h1 class=" count4">
                        0
                    </h1>
                    <p>Offers</p>
                </div>
            </section>
        </div>
    </div>
    <!--state overview end-->

    <div class="row">
        <section class="panel">
        <div class="col-lg-8">
            <!--custom chart start-->
            <div class="border-head">
                <h3>Applications by Month</h3>
            </div>
            <div class="custom-bar-chart">
                <ul class="y-axis">
                    <li><span>100</span></li>
                    <li><span>80</span></li>
                    <li><span>60</span></li>
                    <li><span>40</span></li>
                    <li><span>20</span></li>
                    <li><span>0</span></li>
                </ul>
                <div class="bar">
                    <div class="title">JAN</div>
                    <div class="value tooltips" data-original-title="80%" data-toggle="tooltip" data-placement="top">80%</div>
                </div>
                <div class="bar ">
                    <div class="title">FEB</div>
                    <div class="value tooltips" data-original-title="50%" data-toggle="tooltip" data-placement="top">50%</div>
                </div>
                <div class="bar ">
                    <div class="title">MAR</div>
                    <div class="value tooltips" data-original-title="40%" data-toggle="tooltip" data-placement="top">40%</div>
                </div>
                <div class="bar ">
                    <div class="title">APR</div>
                    <div class="value tooltips" data-original-title="55%" data-toggle="tooltip" data-placement="top">55%</div>
                </div>
                <div class="bar">
                    <div class="title">MAY</div>
                    <div class="value tooltips" data-original-title="20%" data-toggle="tooltip" data-placement="top">20%</div>
                </div>
                <div class="bar ">
                    <div class="title">JUN</div>
                    <div class="value tooltips" data-original-title="39%" data-toggle="tooltip" data-placement="top">39%</div>
                </div>
                <div class="bar">
                    <div class="title">JUL</div>
                    <div class="value tooltips" data-original-title="75%" data-toggle="tooltip" data-placement="top">75%</div>
                </div>
                <div class="bar ">
                    <div class="title">AUG</div>
                    <div class="value tooltips" data-original-title="45%" data-toggle="tooltip" data-placement="top">45%</div>
                </div>
                <div class="bar ">
                    <div class="title">SEP</div>
                    <div class="value tooltips" data-original-title="50%" data-toggle="tooltip" data-placement="top">50%</div>
                </div>
                <div class="bar ">
                    <div class="title">OCT</div>
                    <div class="value tooltips" data-original-title="42%" data-toggle="tooltip" data-placement="top">42%</div>
                </div>
                <div class="bar ">
                    <div class="title">NOV</div>
                    <div class="value tooltips" data-original-title="60%" data-toggle="tooltip" data-placement="top">60%</div>
                </div>
                <div class="bar ">
                    <div class="title">DEC</div>
                    <div class="value tooltips" data-original-title="90%" data-toggle="tooltip" data-placement="top">90%</div>
                </div>
            </div>
            <!--custom chart end-->
        </div>
        </section>
        <div class="col-lg-4">
            <section class="panel">
                <header class="panel-heading">
                    Industries
                </header>
                <div class="panel-body text-center">
                    <canvas id="doughnut" height="300" width="400"></canvas>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <!--user info table start-->
            <section class="panel">
                <div class="panel-body">
                    <a href="#" class="task-thumb">
                        <img src="img/avatar1.jpg" alt="">
                    </a>
                    <div class="task-thumb-details">
                        <h1><a href="#">Angelina Joli</a></h1>
                        <p>Career Advisor</p>
                    </div>
                </div>
                <table class="table table-hover personal-task">
                    <tbody>
                    <tr>
                        <td>
                            <i class=" fa fa-tasks"></i>
                        </td>
                        <td>New Company Contact</td>
                        <td> 02</td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-exclamation-triangle"></i>
                        </td>
                        <td>Priority Student</td>
                        <td> 14</td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-envelope"></i>
                        </td>
                        <td>Inbox</td>
                        <td> 45</td>
                    </tr>
                    <tr>
                        <td>
                            <i class=" fa fa-bell-o"></i>
                        </td>
                        <td>New Notification</td>
                        <td> 09</td>
                    </tr>
                    </tbody>
                </table>
            </section>
            <!--user info table end-->
        </div>
        <div class="col-lg-8">
            <!--work progress start-->
            <section class="panel">
                <div class="panel-body progress-panel">
                    <div class="task-progress">
                        <h1>Activity Tracker</h1>
                        <p>Anjelina Joli</p>
                    </div>
                    <div class="task-option">
                        <select class="styled">
                            <option>Anjelina Joli</option>
                            <option>Tom Crouse</option>
                            <option>Jhon Due</option>
                        </select>
                    </div>
                </div>
                <table class="table table-hover personal-task">
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            Student Engagement
                        </td>
                        <td>
                            <span class="badge bg-important">75%</span>
                        </td>
                        <td>
                            <div id="work-progress1"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>
                            Current Milestones
                        </td>
                        <td>
                            <span class="badge bg-success">43%</span>
                        </td>
                        <td>
                            <div id="work-progress2"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>
                            Total Employment
                        </td>
                        <td>
                            <span class="badge bg-info">67%</span>
                        </td>
                        <td>
                            <div id="work-progress3"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>
                            New Companies
                        </td>
                        <td>
                            <span class="badge bg-warning">30%</span>
                        </td>
                        <td>
                            <div id="work-progress4"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>
                            Priority Students
                        </td>
                        <td>
                            <span class="badge bg-primary">15%</span>
                        </td>
                        <td>
                            <div id="work-progress5"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </section>
            <!--work progress end-->
        </div>
    </div>
@endsection
@section('endScripts')
    {!! HTML::script('js/count.js') !!}
    {!! HTML::script('assets/chart-js/dist/Chart.min.js') !!}
    <script>

        if ($(".custom-bar-chart")) {
            $(".bar").each(function() {
                var i = $(this).find(".value").html();
                $(this).find(".value").html("");
                $(this).find(".value").animate({
                    height: i
                }, 2000);
            });
        }

        var data = {
            labels: [
                "Consulting",
                "Technology",
                "Finance",
                "Healthcare"
            ],
            datasets: [
                {
                    data: [13, 50, 30, 7],
                    backgroundColor: [
                        "#F7464A",
                        "#46BFBD",
                        "#FDB45C"
                    ],
                    hoverBackgroundColor: [
                        "#FF5A5E",
                        "#5AD3D1",
                        "#FFC870"
                    ]
                }]
        };

        var myDoughnutChart = new Chart(document.getElementById("doughnut").getContext("2d"), {
            type:'doughnut',
            data: data{{--,
            tooltipTemplate : "<%if (label){%><%=label%>: <%}%><%= value %>kb",
            animation: false--}}
        });
    </script>
@endsection