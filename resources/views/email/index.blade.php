@extends('layouts.app')
@section('headScripts')
    {!! HTML::script('js/email.js') !!}
    {!! HTML::script('https://apis.google.com/js/client.js?onload=checkAuth') !!}
@endsection
@section('content')
<section id="authorize-div" class="panel">
    <header class="panel-heading">
        Authorize access to Google Gmail API
    </header>
    <div class="panel-body">
        <!-- Button for the user to click to initiate auth sequence -->
        <button class="btn btn-info" id="authorize-button" onclick="handleAuthClick(event)">
            Authorize
        </button>
    </div>
</section>
<div id="mail-div" class="mail-box">
    <aside class="sm-side">
        <div class="user-head">
            <a href="javascript:;" class="inbox-avatar">
                {!! HTML::image('images/profile/' . Auth::user()->id, 'avatar', [
                    'height' => 64,
                    'width' => 60
                ]) !!}
            </a>
            <div class="user-name">
                <h5><a href="#">{!! Auth::user()->name !!}</a></h5>
                <span><a href="#">{!! Auth::user()->email !!}</a></span>
            </div>
            <a href="javascript:;" class="mail-dropdown pull-right">
                <i class="fa fa-chevron-down"></i>
            </a>
        </div>
        <div class="inbox-body">
            <a class="btn btn-compose" data-toggle="modal" href="#myModal">
                Compose
            </a>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Compose</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label  class="col-lg-2 control-label">To</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="inputEmail1" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="col-lg-2 control-label">Cc / Bcc</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="cc" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Subject</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="inputPassword1" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Message</label>
                                    <div class="col-lg-10">
                                        <textarea name="" id="" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                                      <span class="btn green fileinput-button">
                                                        <i class="fa fa-plus fa fa-white"></i>
                                                        <span>Attachment</span>
                                                        <input type="file" multiple=""  name="files[]">
                                                      </span>
                                        <button type="submit" class="btn btn-send">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <ul id="mail" class="inbox-nav inbox-divider">
            <li>
                <a href="#" id="inbox"><i class="fa fa-inbox"></i> Inbox <span class="label label-danger pull-right"></span></a>
            </li>
            <li>
                <a href="#" id="sent"><i class="fa fa-envelope-o"></i> Sent Mail <span class="label label-success pull-right"></span></a>
            </li>
            <li>
                <a href="#" id="important"><i class="fa fa-bookmark-o"></i> Important <span class="label label-warning pull-right"></span></a>
            </li>
            <li>
                <a href="#" id="draft"><i class=" fa fa-external-link"></i> Draft <span class="label label-info pull-right"></span></a>
            </li>
            <li>
                <a href="#" id="spam"><i class=" fa fa-warning"></i> Spam <span class="label label-inverse pull-right"></span></a>
            </li>
            <li>
                <a href="#" id="trash"><i class=" fa fa-trash-o"></i> Trash <span class="label label-inverse pull-right"></span></a>
            </li>
        </ul>
{{--        <ul id="labels" class="nav nav-pills nav-stacked labels-info inbox-divider">
            <li> <h4>Labels</h4> </li>
        </ul>--}}
{{--        <ul class="nav nav-pills nav-stacked labels-info ">
            <li> <h4>Buddy online</h4> </li>
            <li> <a href="#"> <i class=" fa fa-circle text-success"></i> Jhone Doe <p>I do not think</p></a>  </li>
            <li> <a href="#"> <i class=" fa fa-circle text-danger"></i> Sumon <p>Busy with coding</p></a> </li>
            <li> <a href="#"> <i class=" fa fa-circle text-muted "></i> Anjelina Joli <p>I out of control</p></a>
            <li> <a href="#"> <i class=" fa fa-circle text-muted "></i> Jonathan Smith <p>I am not here</p></a>
            <li> <a href="#"> <i class=" fa fa-circle text-muted "></i> Tawseef <p>I do not think</p></a>
            </li>
        </ul>--}}

        <div class="inbox-body text-center">
            <div class="btn-group">
                <a href="javascript:;" class="btn mini btn-primary">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="btn-group">
                <a href="javascript:;" class="btn mini btn-success">
                    <i class="fa fa-phone"></i>
                </a>
            </div>
            <div class="btn-group">
                <a href="javascript:;" class="btn mini btn-info">
                    <i class="fa fa-cog"></i>
                </a>
            </div>
        </div>

    </aside>
    {{--<aside class="lg-side">
        <div class="inbox-head">
            <h3>View Mail</h3>
            <form class="pull-right position" action="#">
                <div class="input-append">
                    <input type="text"  placeholder="Search Mail" class="sr-input">
                    <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
        <div class="inbox-body">
            <div class="heading-inbox row">
                <div class="col-md-8">
                    <div class="compose-btn">
                        <a class="btn btn-sm btn-primary" href="mail_compose.html"><i class="fa fa-reply"></i> Reply</a>
                        <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Print" class="btn  btn-sm tooltips"><i class="fa fa-print"></i> </button>
                        <button title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm tooltips"><i class="fa fa-trash-o"></i></button>
                    </div>

                </div>
                <div class="col-md-4 text-right">
                    <p class="date"> 8:02 PM 12 FEB 2014</p>
                </div>
                <div class="col-md-12">
                    <h4> Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum.</h4>
                </div>
            </div>
            <div class="sender-info">
                <div class="row">
                    <div class="col-md-12">
                        <img alt="" src="img/mail-avatar.jpg">
                        <strong>Mosaddek</strong>
                        <span>[mosaddek73@gmail.com]</span>
                        to
                        <strong>me</strong>
                        <a class="sender-dropdown " href="javascript:;">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="view-mail">
                <p>Faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. </p>
                <p>Consequat risus. Mauris sed congue orci. Donec ultrices <a href="#">flatlab</a>. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec
                    <a href="#">vitae leo at sem lobortis porttitor eu consequat risus</a>. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. </p>
                <p>Modales vulputate urna, vel <a href="#">thevectorlab.net</a>. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. Donec ultrices faucibus rutrum. Phasellus sodales vulputate urna, vel accumsan augue egestas ac. Donec vitae leo at sem lobortis porttitor eu consequat risus. Mauris sed congue orci. </p>
            </div>
            <div class="attachment-mail">
                <p>
                    <span><i class="fa fa-paperclip"></i> 3 attachments &mdash; </span>
                    <a href="#">Download all attachments</a>
                    |
                    <a href="#">View all images</a>
                </p>
                <ul>
                    <li>
                        <a href="#" class="atch-thumb">
                            <img src="img/pro-ac-1.png">
                        </a>

                        <div class="file-name">
                            image-name.jpg
                        </div>
                        <span>12KB</span>


                        <div class="links">
                            <a href="#">View</a> -
                            <a href="#">Download</a>
                        </div>
                    </li>

                    <li>
                        <a href="#" class="atch-thumb">
                            <img src="img/pro-ac-2.png">
                        </a>

                        <div class="file-name">
                            img_name.jpg
                        </div>
                        <span>40KB</span>

                        <div class="links">
                            <a href="#">View</a> -
                            <a href="#">Download</a>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="atch-thumb">
                            <img src="img/pro-ac-1.png">
                        </a>

                        <div class="file-name">
                            img_name.jpg
                        </div>
                        <span>30KB</span>

                        <div class="links">
                            <a href="#">View</a> -
                            <a href="#">Download</a>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="compose-btn pull-left">
                <a class="btn btn-sm btn-primary" href="mail_compose.html"><i class="fa fa-reply"></i> Reply</a>
                <button class="btn btn-sm "><i class="fa fa-arrow-right"></i> Forward</button>
                <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Print" class="btn  btn-sm tooltips"><i class="fa fa-print"></i> </button>
                <button title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm tooltips"><i class="fa fa-trash-o"></i></button>
            </div>
        </div>
    </aside>--}}
    <aside class="lg-side">
        <div class="inbox-head">
            <h3 id="currentLabel">Inbox</h3>
            <form class="pull-right position" action="#">
                <div class="input-append">
                    <input type="text"  placeholder="Search Mail" class="sr-input">
                    <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
        <div class="inbox-body">
            <div class="mail-option">
                <div class="chk-all">
                    <input type="checkbox" id="all" class="mail-checkbox mail-group-checkbox">
                    <div class="btn-group" >
                        <a class="btn mini all" href="#" data-toggle="dropdown">
                            All
                            <i class="fa fa-angle-down "></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#"> None</a></li>
                            <li><a href="#"> Read</a></li>
                            <li><a href="#"> Unread</a></li>
                        </ul>
                    </div>
                </div>

                <div class="btn-group">
                    <a id="refresh" class="btn mini tooltips" href="#" data-toggle="dropdown" data-placement="top" data-original-title="Refresh">
                        <i class=" fa fa-refresh"></i>
                    </a>
                </div>
                <div class="btn-group hidden-phone">
                    <a class="btn mini blue" href="#" data-toggle="dropdown">
                        More
                        <i class="fa fa-angle-down "></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                        <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <a class="btn mini blue" href="#" data-toggle="dropdown">
                        Move to
                        <i class="fa fa-angle-down "></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                        <li><a href="#"><i class="fa fa-ban"></i> Spam</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                    </ul>
                </div>

                <ul class="unstyled inbox-pagination">
                    <li><span>1-50 of 234</span></li>
                    <li>
                        <span id="start">-</span><span id="end"></span> of <span id="totalMessages"></span>
                    </li>
                    <li>
                        <a href="#" class="np-btn"><i class="fa fa-angle-left  pagination-left"></i></a>
                    </li>
                    <li>
                        <a href="#" class="np-btn"><i class="fa fa-angle-right pagination-right"></i></a>
                    </li>
                </ul>
            </div>
            <table class="table table-inbox table-hover" id="mailbox">
                <tbody></tbody>
            </table>
        </div>
    </aside>
</div>
@endsection