@extends('layouts.app')
@section('headStyles')
    <meta name="_token" content="{!! csrf_token() !!}">
    {!! HTML::style(elixir('css/jobs.css')) !!}
@endsection
@section('content')
    <div class="btn-group status">
        <button data-toggle="dropdown" class="btn btn-xs dropdown-toggle status-btn" type="button">
            <span class="caret"></span>
        </button>
        <ul role="menu" class="dropdown-menu">
            @foreach ($status as $entry)
                <li>
                    <a class="update-status" data-status-id="{!! $entry->id !!}" href="#">{!! $entry->name !!}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <section class="panel">
        <header class="panel-heading">Jobs</header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-condensed table-striped display table-bordered table-hover responsive no-wrap" id="hidden-table-info" width="100%">
                    <thead>
                    <th></th>
                    <th>Title</th>
                    <th>Company</th>
                    <th>Job</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th>Actions</th>
                    </thead>
                </table>
            </div>

        </div>
    </section>

    <!-- Right Slidebar start -->
    <div class="sb-slidebar sb-right sb-style-overlay" id="right-sidebar">
        <h5 class="side-title" role="button" data-toggle="collapse" data-parent="#right-sidebar" data-target="#side-details">
            Job Details
            <i class="fa fa-pull-right"></i>
        </h5>
        <ul id="side-details" class="quick-chat-list collapse in">
            <li class="online">
                <div class="media">
                    <div class="media-body">
                        <div id="info-window">
                            <strong id="job-title"></strong>
                            <small id="job-company"></small>
                            <small id="address"></small>
                        </div>
                    </div>
                </div>
            </li>
        </ul>

        <h5 id="location" class="side-title" role="button" data-toggle="collapse" data-parent="#right-sidebar" data-target="#map-wrapper">
            Location
            <i class="fa fa-pull-right"></i>
        </h5>
        <div id="map-wrapper" class="col-xs-12 collapse">
            <div id="map" class="gmaps"></div>
        </div>
        <div class="clearfix"></div>


        <h5 class="side-title" role="button" data-toggle="collapse" data-parent="#right-sidebar" data-target="#side-files">
            Files
            <i class="fa fa-pull-right"></i>
        </h5>
        <ul id="side-files" class="quick-chat-list collapse"></ul>

        <h5 class="side-title" role="button" data-toggle="collapse" data-parent="#right-sidebar" data-target="#side-notes">
            Notes
            <i class="fa fa-pull-right"></i>
        </h5>
        <ul id="side-notes" class="p-task collapse"></ul>
    </div>
    <!-- Right Slidebar end -->
@endsection
@section('endScripts')
    {!! HTML::script('js/jobs.js') !!}
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAX4EmxagkMITQ_Qck-JFRrW-AwHfVX22w&libraries=places"></script>
@endsection