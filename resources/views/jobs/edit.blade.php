@include('jobs/partials/_form', [
    'route' => ['jobs.update', $job->id],
    'types' => $types,
    'file_ids' => $job->files->pluck('id')->toArray(),
    'id' => $job->id,
    'method' => 'PATCH',
    'title' => $job->title,
    'company' => $job->company,
    'location' => $job->location,
    'link' => $job->link,
    'checked' => $job->applied ? 'checked' : '',
    'notes' => $job->notes,
    'type' => 'Update'
]);