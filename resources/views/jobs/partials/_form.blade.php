@section('headStyles')
    <meta name="_token" content="{!! csrf_token() !!}">
    {!! HTML::style('css/jobs-create.css') !!}
@endsection
@extends('layouts.app')
@section('content')
    <section class="panel">
        <section class="panel-heading">
            Add Job
        </section>
        <section class="panel-body">
            {!! Form::open(['route' => $route, 'id' => 'job', 'class' => 'form-horizontal tasi-form', 'role' => 'form']) !!}
            {!! Form::hidden('_method', $method) !!}
            <div class="form-group">
                {!! Form::label("title", "Job Title", ["class" => "col-sm-2 control-label"]) !!}
                <div class="col-sm-10">
                    <div class="input-group">
                        {!! Form::text("title", $title, ['class' => 'form-control', 'id' => 'title', 'autofocus']) !!}
                        <span class="input-group-addon">
                            <i class="fa fa-briefcase"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("company", "Company", ["class" => "col-sm-2 control-label"]) !!}
                <div class="col-sm-10">
                    <div class="input-group">
                        {!! Form::text("company", $company, ["class" => "form-control", "id" => "company"]) !!}
                        <span class="input-group-addon">
                            <i class="fa fa-building"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("location", "Location", ["class" => "col-sm-2 control-label"]) !!}
                <div class="col-sm-10">
                    <div class="input-group">
                        {!! Form::text("location", $location, ["class" => "form-control", "id" => "location"]) !!}
                        <span class="input-group-addon">
                            <i class="fa fa-globe"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("link", "Link", ["class" => "col-sm-2 control-label"]) !!}
                <div class="col-sm-10">
                    <div class="input-group">
                        {!! Form::text("link", $link, ["class" => "form-control", "id" => "link"]) !!}
                        <span class="input-group-addon">
                            <i class="fa fa-external-link"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('files', 'Files', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <select id="multi-select" name="files[]" title="files" multiple="multiple" size="4">
                        @foreach ($types as $type)
                            <optgroup label="{!! $type->name !!}">
                                @foreach ($type->files as $file)
                                    <option value="{!! $file->id !!}" {!! in_array($file->id, $file_ids) ? 'selected' : '' !!}>
                                        {!! $file->original_filename !!}
                                    </option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('notes', 'Notes', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::textarea('notes', $notes, ['class' => 'wysihtml5 form-control', "id" => "notes"]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </section>
    </section>
@endsection
@section('endScripts')
    {!! HTML::script('js/jobs-create.js') !!}
@endsection