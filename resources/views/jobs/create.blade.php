@include('jobs/partials/_form', [
    'action' => './',
    'route' => 'jobs.store',
    'types' => $types,
    'file_ids' => [],
    'id' => '',
    'method' => 'POST',
    'title' => '',
    'company' => '',
    'location' => '',
    'link' => '',
    'checked' => '',
    'notes' => '',
    'type' => 'Add'
]);