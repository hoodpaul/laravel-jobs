<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    {{-- TODO: Replace this with good title for each page --}}
    <title>Jobs Tracker @yield('title')</title>
    {!! HTML::style(elixir('css/all.css')) !!}
@yield('headStyles')

<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    {!! HTML::script(elixir('js/ie.js')) !!}
    <![endif]-->

@yield('headScripts')

<!-- Google Analytics -->
    <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'UA-69478712-1', 'auto');
        ga('send', 'pageview');
    </script>
{!! HTML::script("https://www.google-analytics.com/analytics.js", ['async']) !!}
<!-- End Google Analytics -->
</head>
<body>
@if (Auth::guest())
    @yield('content')
@else
    <section id="container" >

        <!--header start-->
        <header class="header white-bg">
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
            </div>

            <!--logo start-->
            <a href="{!! URL::route('jobs.index') !!}" class="logo">OP<span>37</span></a>
            <!--logo end-->

            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">

                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            @if (!is_null(Auth::user()->image))
                                {!! HTML::image('images/profile/' . Auth::user()->id, 'avatar', ['width' => '29', 'height' => '29']) !!}
                            @endif
                            <span class="username">{!! Auth::user()->name !!}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li><a href="{{ URL::route('profile.index') }}"><i class=" fa fa-suitcase"></i>Profile</a></li>
                            <li><a href="{{ URL::route('profile.edit') }}"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a disabled href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
                            <li><a href="{{ route('logout') }}"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                </ul>
                <!--search & user info end-->
            </div>
        </header>
        <!--header end-->

        <!--sidebar start-->
        <aside>

            <div id="sidebar"  class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu" id="nav-accordion">
                    <li class="sub-menu">
                        <a class="{{ Route::is('dash.index') ? 'active' : '' }}" href="{{ URL::route('dash.index') }}">
                            <i class="fa fa-dashboard"></i>Dashboard
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="{{ URL::route('jobs.index') }}" class="{{ Route::is('jobs.*') ? 'active' : '' }}">
                            <i class="fa fa-briefcase"></i>
                            Jobs
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="{{ URL::route('search.jobs') }}" class="{{ Route::is('search.*') ? 'active' : '' }}">
                            <i class="fa fa-search"></i>
                            <span>Search</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="{{ URL::route('profile.index') }}" class="{{ Route::is('profile.*') ? 'active' : '' }}">
                            <i class="fa fa-eye"></i>
                            <span>Profile</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a class="{{ Route::is('files.*') ? 'active' : '' }}" href="{{ URL::route('files.index') }}">
                            <i class="fa fa-file-text"></i>
                            Files
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a class="{{ Route::is('email.*') ? 'active' : '' }}" href="{{ URL::route('email.index') }}">
                            <i class="fa fa-envelope"></i>
                            Email
                        </a>
                    </li>
                </ul>
                <!-- sidebar menu end-->

            </div>
        </aside>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                @yield('content')
            </section>
        </section>
        <!--main content end-->
    </section>
@endif
{!! HTML::script('js/all.js') !!}
@yield('endScripts')
</body>
</html>
