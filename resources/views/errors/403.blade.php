@include("errors/partials/_error", [
    'code' => 403,
    'title' => 'Forbidden',
    'message' =>
        'You do not have access to this file.'
])