<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $code }} error</title>

    <!-- Bootstrap core CSS -->
    {!! HTML::style("css/bootstrap.min.css") !!}
    {!! HTML::style("css/bootstrap-reset.css") !!}

    <!--external css-->
    {!! HTML::style("assets/font-awesome/css/font-awesome.css") !!}

    <!-- Custom styles for this template -->
    {!! HTML::style("css/style.css") !!}
    {!! HTML::style("css/style-responsive.css") !!}
    {!! HTML::style("css/op37.css") !!}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    {!! HTML::script("js/html5shiv.js") !!}
    {!! HTML::script("js/respond.min.js") !!}
    <![endif]-->
</head>
<body class="body-{{ $code }}">
    <div class="container">
        <section class="error-wrapper">
            <i class="icon-{{ $code }}"></i>
            <h1>{{ $code }}</h1>
            <h2>{{ $title }}</h2>
            <p class="page-{{ $code }}">
                {{ $message }}
            </p>
        </section>
    </div>
</body>
</html>
