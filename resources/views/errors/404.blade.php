@include("errors/partials/_error", [
    'code' => 404,
    'title' => 'Page not found',
    'message' =>
        'Something went wrong or that page doesn\'t exist yet.'
])