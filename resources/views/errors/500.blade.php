@include("errors/partials/_error", [
    'code' => 500,
    'title' => 'Server error',
    'message' =>
        'Looks like something went wrong.'
])