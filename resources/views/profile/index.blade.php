@extends('layouts.app')
@section('content')
    <div class="row">
        @if (Session::has('updated'))
            <div class="col-xs-12">
                <h1>Profile was updated</h1>
            </div>
        @endif
        <aside class="profile-nav col-lg-3">
            @include ('profile/partials/_user', $user)
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    Below are the goals &amp; objectives for {!! $user->name !!}
                </div>
                <div class="panel-body bio-graph-info">
                    <h1>Bio Graph</h1>
                    <div class="row">
                        <div class="bio-row">
                            <p><span>Name: </span>{!! $user->name !!}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Location: </span>{!! $user->location !!}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Email: </span>{!! $user->email !!}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>Date Joined: </span>{!! $user->created_at !!}</p>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="bio-chart">
                                    <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="35" data-fgColor="#e06b7d" data-bgColor="#e8e8e8">
                                </div>
                                <div class="bio-desk">
                                    <h4 class="red">Applications</h4>
                                    <p>Started : 15 July</p>
                                    <p>Deadline : 15 August</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="bio-chart">
                                    <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="63" data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                                </div>
                                <div class="bio-desk">
                                    <h4 class="terques">Follow Ups</h4>
                                    <p>Started : 15 July</p>
                                    <p>Deadline : 15 August</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="bio-chart">
                                    <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="75" data-fgColor="#96be4b" data-bgColor="#e8e8e8">
                                </div>
                                <div class="bio-desk">
                                    <h4 class="green">Interviews</h4>
                                    <p>Started : 15 July</p>
                                    <p>Deadline : 15 August</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="bio-chart">
                                    <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="50" data-fgColor="#cba4db" data-bgColor="#e8e8e8">
                                </div>
                                <div class="bio-desk">
                                    <h4 class="purple">Offers</h4>
                                    <p>Started : 15 July</p>
                                    <p>Deadline : 15 August</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>
@endsection
@section('endScripts')
    {!! HTML::script('assets/jquery-knob/js/jquery.knob.js') !!}
    <script>
        $(".knob").knob();
    </script>
@endsection