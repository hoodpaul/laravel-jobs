@extends('layouts.app')
@section('content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            @include("profile/partials/_user", $user)
        </aside>
        <aside class="profile-info col-lg-9">
            <section>
                <div class="panel panel-primary">
                    <div class="panel-heading"> Profile Info</div>
                    <div class="panel-body">
                        {!! Form::model($user, [
                                'method' => 'PUT',
                                 'route' => [
                                     'profile.update', $user->id
                                 ],
                                 'class' => 'form-horizontal',
                                 'role' => 'form'
                             ]
                         ) !!}
                        {!! Form::hidden('type', 'profile') !!}
                        <div class="form-group">
                            {!! Form::label('name', "Name", ["class" => "col-lg-2 control-label"]) !!}
                            <div class="col-lg-6">
                                {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', "Email", ["class" => "col-lg-2 control-label"]) !!}
                            <div class="col-lg-6">
                                {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('location', "Location", ["class" => "col-lg-2 control-label"]) !!}
                            <div class="col-lg-6">
                                {!! Form::text('location', $user->location, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </section>
            <section>
                <div class="panel panel-primary">
                    <div class="panel-heading"> Set New Password</div>
                    <div class="panel-body">
                        {!! Form::open([
                                'route' => ['profile.update', $user],
                                'method' => 'PUT',
                                'class' => 'form-horizontal',
                                'role' => 'form'
                            ])
                        !!}
                            {!! Form::hidden('type', 'password') !!}
                            <div class="form-group">
                                {!! Form::label('old_password', 'Current Password', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::password('old_password', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('password', 'New Password', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('password', 'Confirm New Password', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Save', ['class' => 'btn btn-info']) !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <section>
                <div class="panel panel-primary">
                    <div class="panel-heading"> Set New Avatar</div>
                    <div class="panel-body">
                        {!! Form::open([
                                'route' => 'files.store',
                                'method' => 'POST',
                                'class' => 'form-horizontal',
                                'role' => 'form',
                                'files' => true
                            ])
                        !!}
                        {!! Form::hidden('type', 'avatar') !!}
                            <div class="form-group">
                                {!! Form::label('avatar', 'Change Avatar', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-6">
                                    {!! Form::file('avatar',['class' => 'file-pos']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    {!! Form::submit('Save', ['class' => 'btn btn-info']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </section>
        </aside>
    </div>
@endsection
@section('endScripts')
@endsection