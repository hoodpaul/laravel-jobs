<section class="panel">
    <div class="user-heading round">
        <a href="{{ URL::route('profile.index') }}">
            {!! HTML::image('images/profile/' . $user->id, 'avatar') !!}
        </a>
        <h1>{!! $user->name !!}</h1>
        <p>{!! $user->email !!}</p>
        <p>{!! $user->location !!}</p>
    </div>
    <ul class="nav nav-pills nav-stacked">
        <li class="{!! Route::is('profile.index') ? 'active' : '' !!}"><a href="{{ URL::route('profile.index') }}"> <i class="fa fa-user"></i> Profile</a></li>
        <li class="{!! Route::is('profile.edit') ? 'active' : '' !!}"><a href="{!! URL::route('profile.edit') !!}"> <i class="fa fa-edit"></i> Edit profile</a></li>
    </ul>
</section>