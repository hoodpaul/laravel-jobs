var Script = function () {

    //    sidebar dropdown menu auto scrolling
    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

    //  Sidebar toggle.
    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });

    // custom scrollbar
    $("#sidebar").niceScroll({
        styler: "fb",
        cursorcolor: "#e8403f",
        cursorwidth: '3',
        cursorborderradius: '10px',
        background: '#404040',
        spacebarenabled: false,
        cursorborder: ''
    });

    $("html").niceScroll({
        styler: "fb",
        cursorcolor: "#e8403f",
        cursorwidth: '6',
        cursorborderradius: '10px',
        background: '#404040',
        spacebarenabled:false,
        cursorborder: '',
        zindex: '1000'
    });

    //    tool tips
    $('.tooltips').tooltip();

    //    popovers
    $('.popovers').popover();

    (function() {

        // Load google analytics.
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.async = true;
        s.src = '//api.usersnap.com/load/'+
            'b1052802-22a1-4ad6-981b-dc1ec7b98e07.js';
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);

        // Used for showing status messages & errors.
        function showMessage(id, toast) {
            var parent = document.getElementById(id);
            var init = function() {
                if (parent !== null) {
                    for (var i = 0; i < parent.children.length; i++)
                        toast(parent.children[i].textContent);
                }
            };
            return {
                parent: parent,
                init: init
            };
        }

        // Check for errors messages.
        showMessage("errors", toastr.error).init();

        // Check for status messages.
        showMessage("status", toastr.status).init();
    }());
}();
