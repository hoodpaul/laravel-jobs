
// add submit listener for showing the modal
document.addEventListener('submit', function(ele) {
    ele.preventDefault();

    $("#myModal6").modal('show');
    $(".btn-danger").on('click', function() {
        ele.target.submit();
    });
});

// page load listener to check for info messages
window.onload = function() {

    // get the alerts dialog
    var alerts = document.getElementsByClassName("alert");

    // remove the alerts after 2 seconds
    if (alerts.length !== 0) {
        window.setTimeout(function () {
            $(alerts).fadeOut("slow");
        }, 2000);
    }
};