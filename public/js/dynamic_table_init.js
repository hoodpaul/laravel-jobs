function fnFormatDetails (oTable, nTr) {

    // clone the hidden form
    var ele = nTr.getElementsByClassName('hidden-content')[0].cloneNode(true);

    // remove hidden class
    ele.className = "";

    // return form html
    return ele.outerHTML;
}

$(document).ready(function() {

    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement( 'th' );
    nCloneTh.setAttribute("style", "width: 19px");
    var nCloneTd = document.createElement( 'td' );
    nCloneTd.innerHTML = '<img src="img/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each( function () {
        this.insertBefore( nCloneTh, this.childNodes[0] );
    } );

    $('#hidden-table-info tbody tr').each( function () {
        this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    } );

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [ 0 ]
        }],
        "aaSorting": [[4, 'desc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $(document).on('click','#hidden-table-info tbody td img',function (e) {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr))  {
            this.src = "img/details_open.png";
            oTable.fnClose( nTr );
        } else {
            this.src = "img/details_close.png";
            oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
    });

    $("#hidden-table-info tbody td img")[0].click();
} );