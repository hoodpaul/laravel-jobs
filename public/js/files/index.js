
// Get the csrf token & store in an object.
var token = {
    "name": "_token",
    "value": document.getElementsByName("_token")[0]
        .getAttribute("content")
};

var oTable = $('#hidden-table-info').dataTable({
    ajax: {
        url: 'files/all',
        dataSrc: ''
    },
    columns: [
        {data: 'original_filename'},
        {data: 'created_at'}, {
            data: function(file) {

                // Size outputs.
                var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];

                // Size of the file.
                var fileSize = parseInt(file.size);

                // Index of the size output starting at -1.
                var index = 0;

                // Convert the file size to human readable output.
                while (fileSize > 1024 && index < sizes.length) {

                    // Divide the file size by 1024 bytes.
                    fileSize /= 1024;

                    // Increment the index.
                    index += 1;
                }
                return Math.round(fileSize) + ' ' + sizes[index];
            },
            type: 'file-size-pre'
        }, {
            data: function (file) {

                // Clone the hidden file type button.
                var btnGroup = document.getElementsByClassName('type')[0]
                    .cloneNode(true);

                // Remove the button id.
                btnGroup.removeAttribute('id');

                // Set the button text.
                btnGroup.firstElementChild.textContent = file.kind.name;

                // Add class name to both of the buttons.
                var className = function () {
                    switch (parseInt(file.kind.id)) {
                        case 2:
                            return 'btn-primary';
                        case 4:
                            return 'btn-success';
                        default:
                            return 'btn-default';
                    }
                }();
                btnGroup.children[0].className += ' ' + className;
                btnGroup.children[1].className += ' ' + className;
                return btnGroup.outerHTML;
            }
        }, {
            data: function (file) {
                var input = document.createElement("input");
                input.value = file.active;
                input.type = "checkbox";
                input.name = "active";
                input.className = "active";

                // Set the checked attribute if it is active.
                if (file.active == 1)
                    input.setAttribute('checked', '1');
                return input.outerHTML;
            },
            type: 'dom-checkbox'
        }, {
            data: function (file) {

                // Wrapper for the button group.
                var actions = document.createElement('div');

                // Append the view file button.
                actions.appendChild(function () {
                    var viewBtn = document.createElement('a');
                    viewBtn.className = "btn btn-xs btn-info view";
                    viewBtn.href = './files/' + file.id + '/' +
                        file.original_filename;
                    viewBtn.target = "_blank";
                    viewBtn.appendChild(function () {
                        var icon = document.createElement('i');
                        icon.className = 'fa fa-eye';
                        return icon;
                    }());
                    return viewBtn;
                }());

                // Append the delete file button.
                actions.appendChild(function () {
                    var deleteBtn = document.createElement('btn');
                    deleteBtn.className = 'btn btn-xs btn-danger delete';
                    deleteBtn.appendChild(function () {
                        var icon = document.createElement('i');
                        icon.className = 'fa fa-ban delete';
                        return icon;
                    }());
                    return deleteBtn;
                }());
                return actions.outerHTML;
            },
            orderable: false
        }
    ],
    columnDefs: [{
        className: 'dt-center',
        targets: [-1, -2, -3, -4]
    }],
    order: [1, 'desc'],
    responsive: {
        details: false
    },
    rowId: 'id'
});

// Check if the files table has been clicked.
oTable.on('click', 'tr', function(e) {

    // Get the target's class name.
    var className = e.target.className;

    // Function checks if the parameter is the the class name.
    var hasClass = function(searchClass) {
        return className.indexOf(searchClass) !== -1;
    };

    // Returns true if the class name should trigger an ajax call.
    var updateClasses = function(classes) {
        for (var i = 0; i < classes.length; i++) {
            if (hasClass(classes[i]))
                return true;
        }
        return false;
    };

    // Saves the current row.
    var row = this;

    // Update active status, file type, or delete the file.
    if (updateClasses(['update-type', 'active', 'delete'])) {
        e.preventDefault();

        // Start of the request.
        var request = './files/' + this.id + '?_token=' + token.value + '&';

        // Request method.
        var requestMethod = 'PUT';

        // Setup the xhr request.
        var xhr = new XMLHttpRequest();

        // Append data to the request based on the element that was clicked.
        if (hasClass('active')) {

            // Set the value to 0 if the checkbox is not checked.
            var active = e.target.checked ? 1 : 0;

            // Append the value to the request url.
            request += 'active=' + active;

            // Add custom event function for active update.
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {

                    // Check if everything updated correctly.
                    switch (xhr.status) {
                        case 200:

                            // Get the returned data.
                            var data = JSON.parse(xhr.responseText);

                            // Redraw the row.
                            oTable.api().row(row).data(data);
                            break;
                        default:
                            toastr.error(data.message);
                    }
                }
            };
        } else if (hasClass('update-type')) {

            // Append the kind id to the request url.
            request += 'file_type=' + e.target.getAttribute('data-type-id');

            // Add custom event listener for the file kind update.
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {

                    // Check if the kind was updated correctly.
                    switch (xhr.status) {
                        case 200:

                            // Get the returned data.
                            var data = JSON.parse(xhr.responseText);

                            // Redraw the row.
                            oTable.api().row(row).data(data);
                            break;
                        default:
                            toastr.error(xhr.responseText);
                    }
                }
            };
        } else if (hasClass('delete')) {

            // Change the request method.
            requestMethod = 'DELETE';

            //
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {

                    //
                    switch (xhr.status) {
                        case 200:
                            toastr.success(xhr.responseText);

                            // Remove the row from the table.
                            oTable.api().row(row).remove().draw();
                            break;
                        default:
                            toastr.error(xhr.responseText);
                    }
                }
            };
        }

        // Send the ajax call.
        xhr.open(requestMethod, request);
        xhr.send();
    }
});

// Triggered when a row has been double clicked.
oTable.on('dblclick', 'tr', function(e) {

    // Check if a table header was clicked.
    if (e.target.tagName !== 'TH') {

        // Get the row data.
        var rowData = oTable.api().row(this).data();

        // Open the file is it is not the header.
        if (this !== undefined) {

            // Build the file url.
            var url = './files/' + rowData.id + '/' + rowData.original_filename;

            // Open the file.
            window.open(url);
        }
    }
});

// https://datatables.net/plug-ins/sorting/custom-data-source/dom-checkbox
$.fn.dataTable.ext.order['dom-checkbox'] = function (settings, col) {
    return this.api().column(col, {order:'index'} ).nodes().map(function (td, i) {
        return $('input', td).prop('checked') ? '1' : '0';
    });
};

// https://datatables.net/plug-ins/sorting/file-size
$.fn.dataTable.ext.type.order['file-size-pre'] = function (data) {
    var matches = data.match( /^(\d+(?:\.\d+)?)\s*([a-z]+)/i );
    var multipliers = {
        B:  1,
        KB: 1000,
        KIB: 1024,
        MB: 1000000,
        MIB: 1048576,
        GB: 1000000000,
        GIB: 1073741824,
        TB: 1000000000000,
        TIB: 1099511627776,
        PB: 1000000000000000,
        PIB: 1125899906842624
    };

    if (matches) {
        var multiplier = multipliers[matches[2].toLowerCase()];
        return parseFloat( matches[1] ) * multiplier;
    } else {
        return -1;
    }
};


Dropzone.options.myAwesomeDropzone = {
    success: function(file) {

        // Data sent back from the server.
        var data = JSON.parse(file.xhr.responseText);

        // Add the new file to the table.
        oTable.api().row.add(data).draw();

        // File has been uploaded.
        toastr.success(data.original_filename + " has been uploaded.");
    },

    error: function() {
        toastr.error("Something went wrong. Please try again.");
    }
};