var CLIENT_ID = "301431777316-hv21omdo88q30dpdlpbe008jbebkbgbh.apps.g" +
    "oogleusercontent.com";

var SCOPES = ["https://www.googleapis.com/auth/gmail.readonly"];

/**
 * Check if current user has authorized this application.
 */
function checkAuth() {
    gapi.auth.authorize({
        'client_id': CLIENT_ID,
        'scope': SCOPES.join(' '),
        'immediate': true
    }, handleAuthResult);
}

/**
 * Handle response from authorization server.
 */
function handleAuthResult(authResult) {
    var authorizeDiv = document.getElementById("authorize-div");
    var mailDiv = document.getElementById("mail-div");

    // Check if the user is already authorized.
    if (authResult && !authResult.error) {

        // Hide auth UI, then load client library.
        authorizeDiv.style.display = "none";
        mailDiv.style.display = "table";
        loadGmailApi();
    } else {

        // Show auth UI, allow the user to initiate authorization by
        // clicking authorize button.
        authorizeDiv.style.display = "inline-block";
    }
}

/**
 * Initiate auth flow in response to user clicking authorize button.
 */
function handleAuthClick(event) {
    gapi.auth.authorize({
        client_id: CLIENT_ID,
        scope: SCOPES,
        immediate: false
    }, handleAuthResult);
}

/**
 * Load Gmail API client library. List labels once client library is loaded.
 */
function loadGmailApi() {
    gapi.client.load('gmail', 'v1', updateScreen);
}

/**
 * Update the screen.
 */
function updateScreen() {
    listLabels();
    listMessages("INBOX");
}

/**
 * Print all of the system labels.
 */
function listLabels() {

    // System labels.
    var systemLabels = ["INBOX", "SENT", "IMPORTANT", "DRAFT", "TRASH", "SPAM"];

    // Add all of the labels.
    for (var i = 0; i < systemLabels.length; i++) {
        var request = gapi.client.gmail.users.labels.get({
            'userId': 'me',
            'id': systemLabels[i]
        });

        request.execute(function(resp) {

            // Get the number of unread messages from this label.
            var unread = resp.messagesUnread;
            if (unread > 0) {
                document.getElementById(resp.name.toLowerCase())
                    .lastElementChild.textContent = unread;
            }
        });
    }
}

/**
 * Print all email messages.
 */
function listMessages(label) {

    // Clear the current table
    var table = document.getElementById("mailbox");

    // Delete rows until the table is empty
    while (table.rows.length > 0) {

        // Delete the last row.
        table.deleteRow(-1);
    }

    // Update the parent elements class name to active.
    document.getElementById(label.toLowerCase()).parentElement.className = "active";

    // Send a request for a specific label.
    var request = gapi.client.gmail.users.messages.list({
        'userId': 'me',
        'labelIds': label.toUpperCase()
    });

    request.execute(function(resp) {
        var messages = resp.messages;

        // Go through each message in the response.
        if (messages && messages.length > 0) {
            for (var i = 0; i < messages.length; i++) {
                getMessage(messages[i].id);
            }
        } else {
            alert("No messages found.");
        }
    });
}

/**
 * Get specific message information.
 */
function getMessage(id) {
    var request = gapi.client.gmail.users.messages.get({
        'userId': 'me',
        'id': id
    });

    request.execute(function(resp) {
        addMessage(resp);
    });
}

/**
 * Append table row to the email table.
 */
function addMessage(message) {

    // Mailbox table.
    var table = document.getElementById("mailbox");

    // Message headers used for getting information about the sender.
    var headers = message.payload.headers;

    var row = table.insertRow(table.rows.length);

    if (message.labelIds.indexOf("UNREAD") !== -1) {
        row.className = "unread";
    }

    for (var i = 0; i <= 5; i++) {
        var cell = row.insertCell(i);

        if (i == 0 || i == 1)
            cell.className = "inbox-small-cells";
        else
            cell.className = "view-message";

        switch (i) {
            case 0:
                var child = document.createElement("input");
                child.type = "checkbox";
                child.className = "mail-checkbox";
                cell.appendChild(child);
                break;
            case 1:
                var child = document.createElement("i");
                child.className = "fa fa-star";
                cell.appendChild(child);
                break;
            case 2:
                cell.className += " dont-show";

                // Get the sender of the email.
                for (var j = 0; j < headers.length; j++) {

                    // Check if the header contains the from name.
                    if (headers[j].name === "From") {
                        cell.textContent = headers[j].value
                            .replace(/\s<(.*)>|"/g, "");

                        // Stop looping through the headers.
                        break;
                    }
                }
                break;
            case 3:

                // Get the subject of the email
                for (var k = 0; k < headers.length; k++) {

                    // Check if the header contains the subject.
                    if (headers[k].name == "Subject") {
                        cell.textContent = headers[k].value;
                        break;
                    }
                }
                break;
            case 4:
                cell.className += " inbox-small-cells";
                break;
            case 5:
                cell.className += " text-right";

                // Convert the date to an integer.
                var time = parseInt(message.internalDate);

                // Convert the date to date object.
                var date = new Date(time);

                // Check if the time is older than 1 day ago.
                if ((Date.now() - time) / 1000 / 60 / 60 / 24 < 1) {
                    cell.textContent = date.toLocaleTimeString()
                        .replace(/:\d+\s/, " ");
                } else {
                    cell.textContent = date.toLocaleDateString();
                }
                break;
        }
    }
}

function addClickListener(element, handler) {
    element.addEventListener("click", handler);
}

window.onload = function() {
    addClickListener(document.getElementById("mail"), function(e) {

        // Get the active item from the current label id.
        var labelTitle = document.getElementById("currentLabel");
        var activeId = labelTitle.textContent.toLowerCase();


        // Check if the clicked label is already active.
        if (activeId !== e.target.id) {

            // Remove the active class from the current item's parent.
            document.getElementById(activeId).parentElement.className = "";

            // Add the active class to the current element's parent.
            e.target.parentElement.className = "active";

            // Update the label title element.
            labelTitle.textContent = e.target.id;

            // Update the labels.
            listLabels();

            // Update the message list.
            listMessages(e.target.id);
        }
    });

    addClickListener(document.getElementById("all"), function(e) {

        // Get all mail checkboxes.
        var checkboxes = document.getElementsByClassName("mail-checkbox");

        // Check or uncheck each mail checkbox.
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = e.target.checked;
        }
    });

    addClickListener(document.getElementById("refresh"), function(e) {
        listLabels();
        listMessages(document.getElementById("currentLabel").textContent);
    });
};