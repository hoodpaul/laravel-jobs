
/** Will be initialized after the script is loaded. */
var map;

/** Marker used to show the job location. */
var marker;

/** Service used to search for the location. */
var service;

/** Map dom element. */
var mapElement = document.getElementById("map");

/** Map options. */
var mapOptions = {
    center: {
        lat: 37.09024,
        lng: -95.71289136
    },
    mapTypeControl: false,
    panControl: false,
    rotateControl: false,
    scaleControl: false,
    scrollwheel: false,
    streetViewControl: false,
    zoom: 2,
    zoomControl: false
};

/**
 * Setup the google map.
 */
function init() {

    // Initialize the google map with original settings.
    map = new google.maps.Map(mapElement, mapOptions);

    // Setup the places service.
    service = new google.maps.places.PlacesService(map);
}

/**
 * Used to search for job location.
 */
function searchLocation(location) {

    // Check if the map is setup.
    if (map === undefined)
        init();

    // Clear the markers if any exist.
    if (marker !== undefined)
        marker.setMap(null);

    // Service text search options.
    var serviceOptions = {
        query: location
    };

    // Search for the job location.
    service.textSearch(serviceOptions, searchCallback);
}

/**
 * Callback function used to draw the marker on the map.
 * @param results
 * @param status
 */
function searchCallback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        map.fitBounds(results[0].geometry.viewport);
        marker = new google.maps.Marker({
            position: results[0].geometry.location,
            map: map
        });
    }
}