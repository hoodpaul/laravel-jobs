
/** Variables used for this page. **/
var module = function() {

    // Controller for the right sidebar.
    var sidebar = null;

    // Token used for submitting the forms.
    var token = null;

    // Method spoofing.
    var method = null;

    // Set the sidebar variable.
    var setSidebar = function() {
        this.sidebar = new $.slidebars();
    };

    // Set the token variable.
    var setToken = function() {
        this.token = document.getElementsByName("_token")[0].content;
    };

    // Set the method variable.
    var setMethod = function(method) {
        this.method = method;
    };

    // Setup the objects global variables.
    var init = function() {
        this.setSidebar();
        this.setToken();
    };

    return {
        sidebar: sidebar,
        token: token,
        method: method,
        setSidebar: setSidebar,
        setToken: setToken,
        setMethod: setMethod,
        init: init
    };
}();

$(document).ready(function() {

    // Shows tooltip for all buttons on the page.
    $("[data-toggle='tooltip']").tooltip({
        trigger: "hover focus"
    });

    // Setup the jobs page global variable.
    module.init();
});

function getColorClass(index) {
    switch (index === -1 ? parseInt((Math.random() * 6) + 1) : index) {
        case 1:
            return "btn-info";
        case 2:
            return "btn-warning";
        case 3:
            return "btn-default";
        case 4:
            return "btn-primary";
        case 5:
            return "btn-danger";
        case 6:
            return "btn-success";
    }
}


function deleteHandler(id) {

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {

            if (xhr.status === 200) {
                var data = JSON.parse(xhr.responseText);
                toastr.success(data.title + ' was successfully deleted. Click here to undo.', '', {
                    "closeButton": true,
                    "debug": false,
                    "extendedTimeOut": "5000",
                    "hideDuration": "1000",
                    "hideEasing": "linear",
                    "hideMethod": "fadeOut",
                    "newestOnTop": false,
                    "onclick": function() {
                        alert("undo job delete coming soon");
                    },
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "showEasing": "swing",
                    "showMethod": "fadeIn",
                    "timeOut": "10000"
                });

                // Row to delete.
                var row = document.getElementById(data.id);
                oTable.api().row(row).remove().draw();
            } else {
                toastr.error("Something went wrong.");
            }
        }
    };
    xhr.open("DELETE", "./jobs/" + id + "?_token=" + module.token);
    xhr.send();
}

function statusHandler(row, e) {

    // Get the new status id.
    var status = parseInt(e.getAttribute("data-status-id"));

    // Get the parent button.
    var btn = e.parentElement.parentElement.previousElementSibling;

    // Set the submission method.
    module.setMethod("PATCH");

    // Attempt to send the update via ajax.
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status !== 200) {
            toastr.error("Something went wrong. Please try again");
        }
    };
    xhr.open("POST", "./jobs/" + row.id + "?status_id=" + status
        + "&_token=" + module.token + "&_method=" + module.method);
    xhr.send();

    // Remove the last class name.
    btn.classList.remove(
        btn.classList.item(
            btn.classList.length - 1
        )
    );

    // Add the new class.
    btn.className += " " + getColorClass(status);
    btn.innerHTML = e.textContent + ' <span class="caret"></span>';
}

function statusBtn(job) {

    // Clone the hidden status button.
    var status = document.getElementsByClassName("status")[0]
        .cloneNode(true);

    // Add hidden class with the status id to the wrapper.
    var hide = document.createElement("span");
    hide.className = "hide";
    hide.appendChild(document.createTextNode(job.status_id));
    status.appendChild(hide);

    // Update the button wrapper class name.
    status.className = "btn-group";

    // Class to add to the button.
    var className = getColorClass(parseInt(job.status_id));

    // Update the button text & class.
    var btn = status.children[0];
    btn.setAttribute("data-job-id", job.id);
    btn.className += " " + className;
    btn.innerHTML = job.status.name + " " + btn.innerHTML;
    return status;
}

function iconButton (type, btnClass, iconClass, tooltip) {

    // Create the button element.
    var button = document.createElement(type);
    button.className = "btn " + btnClass;
    button.setAttribute("data-tooltip", "tooltip");
    button.title = tooltip;

    // Create the icon element and append it to the button.
    var icon = document.createElement("i");
    icon.className = "fa " + iconClass;
    button.appendChild(icon);
    return button;
}

var oTable = $('#hidden-table-info').dataTable({
    ajax: {
        url: 'jobs/1',
        dataSrc: ''
    },
    columns: [
        {
            className: 'dt-center show-details',
            data: function() {
                var button = document.createElement("button");
                button.className = "btn btn-xs btn-success details";
                button.appendChild(function() {
                    var icon = document.createElement("i");
                    icon.className = "fa fa-plus details";
                    return icon;
                }());
                return button.outerHTML;
            },
            orderable: false
        },
        { data: function(job) {
            return job.title.match(/(\w+\s?){0,3}/)[0].trim();
        } },
        { data: 'company' },
        {
            className: 'p-name not-desktop',
            data: function(job) {
                var title = job.title.match(/(\w+\s?){0,2}/)[0].trim();
                return '<div class="col-xs-12"><span>' + title + '</span></div>' +
                    '<div class="col-xs-12"><small>' + job.company +
                    '</small></div>';
            }
        },
        { data: 'location' },
        {
            className: "dt-center",
            data: function(job) {
                return statusBtn(job).outerHTML;
            }}, {
            className: 'dt-center',
            data: function(job) {

                // Edit button,
                var editBtn = iconButton("a", "btn-xs btn-primary edit", "fa-edit", "Edit job");
                editBtn.href = "./jobs/" + job.id + "/edit";

                // Delete button.
                var deleteBtn = iconButton("button", "btn-xs btn-danger delete", "fa-ban delete", "Delete job");

                // Add the delete url to the delete button.
                deleteBtn.setAttribute("action", "./op37.com/jobs/" + job.id);

                // Create element for sorting by jobs with & without links.
                var hasLink = job.link === "" ? 0 : 1;
                var hiddenSpan = document.createElement("span");
                hiddenSpan.className = "hide";
                hiddenSpan.appendChild(document.createTextNode(hasLink));

                // Button for viewing the external job posting.
                var type = hasLink === 1 ? "a" : "button";
                var className = (hasLink === 1 ? "btn-info" : "btn-default") + " link";
                var linkBtn = iconButton(type, "btn-xs " + className, "fa-hand-o-right link", "View posting");
                linkBtn.appendChild(hiddenSpan);

                // Add the link elements if the job has a valid link.
                if (hasLink === 1) {
                    linkBtn.href = job.link;
                    linkBtn.title = "View posting";
                    linkBtn.target = "_blank";
                    linkBtn.setAttribute("data-toggle", "tooltip");
                }

                // Create parent div for all of the buttons.
                var actions = document.createElement("div");
                actions.appendChild(linkBtn);
                actions.appendChild(editBtn);
                actions.appendChild(deleteBtn);

                // Return the outer html from the div.
                return actions.outerHTML;
            }}
    ],
    columnDefs: [{
        className: 'desktop',
        targets: [1, 2, 4, 5]
    }, {
        responsivePriority: 1,
        targets: [0, -1]
    }, {
        responsivePriority: 2,
        targets: 1
    }, {
        width: '50px',
        targets: [0, -1]
    }],
    dom: "<'row'<'col-xs-6'><'col-xs-6'>>" +
    "<'row'<'col-xs-6'l><'col-xs-6'<'pull-right'f>>>" +
    "<'row'<'col-xs-12'tr>>" +
    "<'row'<'col-xs-5'i><'col-xs-7'p>>",
    order: [1, 'asc'],
    pagingType: "simple",
    responsive: {
        details: false
    },
    rowId: 'id'
});

oTable.on('init.dt', function() {

    // Insert add new job button.
    document.getElementsByClassName("col-xs-6")[0].appendChild(function() {
        var button = document.createElement("a");
        button.className = "btn btn-primary";
        button.href = "./jobs/create";
        button.appendChild(function() {
            var icon = document.createElement("i");
            icon.className = "fa fa-plus";
            return icon;
        }());
        button.appendChild(document.createTextNode(" Add Job"));
        return button;
    }());
});

oTable.on('click', 'tr', function(e) {

    // Check if a header was clicked.
    if (e.target.tagName !== "TH") {

        var className = e.target.className;

        var buttonClicked = function(searchClass) {
            return className.indexOf(searchClass) !== -1;
        };

        // Check if the delete button was clicked.
        if (buttonClicked("delete")) {
            e.preventDefault();
            deleteHandler(this.id);
        }

        // Check if an update status button was clicked.
        else if (buttonClicked("update-status")) {
            e.preventDefault();
            statusHandler(this, e.target);
        }

        // Check if an open details button was clicked.
        else if (buttonClicked("details")) {
            e.preventDefault();
            detailHandler(this, e.target);
        }
    }
});

function detailHandler(row) {

    // Row specific data.
    var data = oTable.fnGetData(row);

    document.body.appendChild(function() {
        var div = document.createElement("div");
        div.id = "modal-backdrop";
        div.className = "modal-backdrop fade in";

        // Event listener for the backdrop.
        div.addEventListener('click', function (e) {
            document.body.removeChild(div);
            module.sidebar.close();
        });
        return div;
    }());

    // Update the job details.
    document.getElementById("job-title").textContent = data.title;
    document.getElementById("job-company").textContent = data.company;
    document.getElementById("address").textContent = data.location;

    // Create click event listener to load the google map location.
    document.getElementById("location").addEventListener('click', function() {
        searchLocation(data.location);
    });

    // Holds the files associated with the job.
    var files = document.getElementById("side-files");

    // Remove previous files.
    while (files.hasChildNodes()) {
        files.removeChild(files.firstChild);
    }

    // Check if the job has files associated with it.
    if (data.files.length > 0) {

        // Add current job files.
        for (var i = 0; i < data.files.length; i++) {
            files.appendChild(function () {

                // Create list item.
                var li = document.createElement("li");
                li.className = "online";
                li.appendChild(function () {

                    // Media element.
                    var div = document.createElement("div");
                    div.className = "media";
                    div.appendChild(function () {

                        // Media thumbnail image.
                        var a = document.createElement("a");
                        a.className = "pull-left media-thumb";
                        a.appendChild(function () {
                            var img = new Image();
                            img.src = "img/file-search/pdf.png";
                            img.alt = "pdf";
                            return img;
                        }());
                        return a;
                    }());

                    // Media body with original filename.
                    div.appendChild(function () {
                        var div = document.createElement("div");
                        div.className = "media-body";
                        div.appendChild(function() {
                            var a = document.createElement("a");
                            a.href = "./files/" + data.files[i].id + '/'
                                + data.files[i].original_filename;
                            a.appendChild(document.createTextNode(
                                data.files[i].original_filename));
                            return a;
                        }());
                        return div;
                    }());
                    return div;
                }());
                return li;
            }());
        }
    }

    // Add the add new files button.
    files.appendChild(function() {

        // Create list item.
        var li = document.createElement("li");
        li.appendChild(function() {
            var a = document.createElement("a");
            a.href = "./jobs/" + data.id + "/edit?startIndex=1";
            a.appendChild(document.createTextNode("Add Files"));
            return a;
        }());
        return li;
    }());

    // Update the notes section.
    var notes = document.getElementById("side-notes");
    notes.innerHTML = "<li>" + (data.notes === "" ? 'empty' : data.notes)
        + "</li>";

    // Open the sidebar.
    module.sidebar.open('right');
}