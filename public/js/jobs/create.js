$(document).ready(function() {

    // Wysiwyg editor for the notes section.
    $("#notes").wysihtml5({
        'font-styles': false,
        'image': false,
        'lists': false,
        'link': false
    });

    // Used to select which files are associated with a job.
    $("#multi-select").multiSelect({
        selectableHeader: '<h4>All Files</h4>',
        selectionHeader: '<h4>Job Files</h4>'
    });
});