require 'compass/import-once/activate'
# Require any additional compass plugins here.


http_path = "/"
css_dir = "public/css"
sass_dir = "public/scss"
images_dir = "public/img"
javascripts_dir = "public/js"

output_style = :expanded

relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
line_comments = true

preferred_syntax = :scss
