<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{

    /** glassdoor api id */
    const ID = '47506';

    /** glassdoor api key */
    const KEY = 'erRavMJczgE';

    /** @var string base url for the request to glassdoor api */
    private static $base_url = "http://api.glassdoor.com/api/api.htm?format=json&v=1&t.p=" . self::ID . "&t.k=" . self::KEY;

    /**
     * @param $ip
     * @param $user_agent
     * @return mixed
     */
    public static function getData($ip, $user_agent, $requests = [])
    {

        // add the user ip & user agent to the base url
        $request = sprintf("%s&userip=%s&useragent=%s", self::$base_url, $ip,
            $user_agent);

        // loop through the rest of the request fields
        foreach ($requests as $key => $value) {
            if (!empty($value)) {
                $request .= sprintf("&%s=%s", $key, $value);
            }
        }

        // replace all of the spaces in the request with %20
        $request = str_replace(" ", "%20", $request);

        // return the data from glassdoor api & decode the data
        return json_decode(file_get_contents($request));
    }
}