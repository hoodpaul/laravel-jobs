<?php

namespace App\Library;

/**
 * Created by PhpStorm.
 * User: paul
 * Date: 11/23/15
 * Time: 10:03 AM
 */
class Api
{
    private $id;
    private $key;
    private $base_url;

    public function __construct(array $id, array $key, $base_url, array $required = [])
    {
        $this->id = $id;
        $this->key = $key;
        $this->base_url = sprintf("%s?", $base_url);

        // add the required fields to the base_url
        foreach ([$this->id, $this->key] as $credential) {
            $key = key($credential);
            $value = $credential[$key];
            echo $key . ' ' . $value . '<br />';
        }
        exit;
    }


}