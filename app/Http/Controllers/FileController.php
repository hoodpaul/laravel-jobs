<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use App\File as FileEntry;
use App\FileImage;
use App\FileType;
use File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Storage;
use Redirect;
use Auth;
use Response;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get all of the file types except the avatar file type.
        $file_types = FileType::where('id', '<>',
            config('constants.AVATAR_FILE_TYPE'))->get();
        return view('files.index', compact('file_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Get the file type.
        $type = is_null($request->file('avatar')) ? 'file' : 'avatar';

        // Add the file to the uploads folder.
        $file = $this->addFile($request, $type);

        // Add image & kind information.
        $file = $file->where('file_type', '<>', config('constants.AVATAR_FILE_TYPE'))
            ->where('user_id', $request->user()->id)
            ->where('id', $file->id)
            ->with('image', 'kind')
            ->first();

        // Return the file information.
        return Response::json($file);
    }

    public function update(Request $request, $id)
    {

        // Retrieve the file model.
        $file = FileEntry::find($id);

        // Check if the file was found.
        if ($file) {

            // Update the file model.
            if ($file->update($request->all())) {

                // Return the updated file information with type & image.
                $file = FileEntry::where('user_id', $request->user()->id)
                    ->where('file_type', '<>', config('constants.AVATAR_FILE_TYPE'))
                    ->where('id', $file->id)
                    ->with('image', 'kind')
                    ->first();
                return Response::json($file);
            }
        }

        // Return error message.
        return Response::make("Something went wrong. Please try again", 500);
    }

    /**
     * Display the specified resource.
     *
     * @param $file_id
     * @param $filename
     * @return \Illuminate\Http\Response
     * @internal param $user_id
     * @internal param int $id
     */
    public function show($file_id, $filename)
    {

        // Get file model.
        $file = FileEntry::find($file_id);

        // Check if the user owns the file or if the file is not found.
        if (is_null($file)) {
            return Response::view('errors.404', [], 404);
        } else if (Auth::user()->id != $file->user->id) {
            return Response::view('errors.403', [], 403);
        } else {
            return Response::make(
                File::get(storage_path('uploads') . '/' . $file->filename), 200, [
                    'Content-type' => $file->mime
                ]
            );
        }
    }

    public function showAll(Request $request)
    {

        // All of the user's files.
        $files = FileEntry::where('file_type', '<>', 1)
            ->where('user_id', $request->user()->id)
            ->with('image', 'kind')->get();

        // Json encode the array before returning.
        return json_encode($files);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // Attempt to retrieve file by it's id.
        try {

            // Storage variable.
            $storage = Storage::disk('local');

            // File model.
            $file = FileEntry::findOrFail($id);

            // File contents.
            $contents = File::get(
                sprintf("%s/%s", storage_path('uploads'), $file->filename)
            );

            // Create new array for storing deletion results.
            $file->deleted = (object) [
                'storage' => $storage->delete($file->filename)
            ];

            // Check if file was deleted from storage.
            if ($file->deleted->storage) {

                // Attempt to delete the model.
                $file->deleted->model = $file->delete();

                // Restore the file if the model was not deleted.
                if (!$file->deleted->model)
                    $storage->put($file->filename, $contents);
            }
            return Response::make(
                $file->original_filename . ' was successfully deleted.', 200
            );
        } catch (FileNotFoundException $e) {
            return Response::make("File not found.", 404);
        } catch (ModelNotFoundException $e) {
            return Response::make("File model not found.", 404);
        } catch (Exception $e) {
            return Response::make("Something went wrong.", 500);
        }
    }

    /**
     * @param UploadedFile $file
     * @param $file_type
     * @param $filename
     * @param $id
     * @return static
     */
    private function createFileEntry(UploadedFile $file, $file_type, $filename, $id)
    {

        // File image id.
        $fileImage = FileImage::where(
            "name", $file->getClientOriginalExtension())->value("id");

        return FileEntry::create([
            'active' => null,
            'mime' => $file->getClientMimeType(),
            'original_filename' => $file->getClientOriginalName(),
            'filename' => $filename,
            'file_type' => $file_type,
            'size' => $file->getSize(),
            'user_id' => $id,
            'file_image' =>
                $fileImage ? $fileImage : config('constants.DEFAULT_FILE_IMAGE')
        ]);
    }

    /**
     * @param Request $request
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addFile(Request $request, $type = 'file')
    {

        // User information.
        $user = $request->user();

        // Get the file information.
        $file = $request->file($type);

        // Name of the file to store in the database.
        $filename = sprintf(
            "%s-%s-%s",
            date('YmdHis'),
            uniqid(),
            preg_replace("/ /i", "", $file->getClientOriginalName())
        );

        // Insert the image into local storage.
        Storage::disk('local')->put($filename, File::get($file));

        // Determine the file type
        $file_type = $type == 'file' ? config('constants.OTHER_FILE_TYPE') : config('constants.AVATAR_FILE_TYPE');

        // Check if avatar already exists.
        if ($file_type == 1 && !(is_null($user->image))) {

            // Delete the image from the db & local storage.
            Storage::disk('local')->delete($user->image->filename);
            $user->image->delete();
        }

        // Create a new file entry.
        $newFile = $this->createFileEntry($file, $file_type, $filename, $user->id);

        // Update the user's data or do nothing.
        if ($file_type == 1) {
            $user->avatar_file_id = $newFile->id;

            // Save new user information.
            $user->save();

            // Redirect back to the index.
            return Redirect::route('profile.index');
        }

        // Return the file entry;
        return $newFile;
    }
}
