<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Soon;
use Input;
use Redirect;
use View;

class SoonController extends Controller
{

    /** form validation rules */
    protected $rules = [
        'email' => 'required|max:255'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('coming-soon.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // validate the input
        $this->validate($request, $this->rules);

        // message to return to the user
        $message = "We'll let you know when we launch.";

        // attempt to insert future user into the db
        try {

            // insert the new user
            Soon::create(Input::all());
            $message = "Thank you! $message";
        } catch (QueryException $e) {

            // user is already in the database
            $message = "You're already signed up! $message";
        }
        return Redirect::refresh()->with('message', $message);
    }

}
