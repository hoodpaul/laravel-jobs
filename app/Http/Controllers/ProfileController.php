<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\FileEntry;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use DB;
use Hash;
use View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ProfileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        // include user path to avatar image
        return view('profile.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Request $request)
    {
	// 
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param User $user
     * @internal param int $id
     */
    public function edit(Request $request)
    {

        // get user information
        $user = $request->user();
        return view('profile.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User $user
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param User $user
     * @internal param int $id
     */
    public function update(Request $request, User $user)
    {

        // get form submission type
        $type = Input::get('type');

        // update user profile information
        if ($type == "profile") {

            // get desired input fields
            $input = (array_except(Input::all(), [
                'type', '_method', '_token'
            ]));

            // update the user fields that match the input
            foreach ($input as $key => $value) {
                $user[$key] = $value;
            }

            // updated user information
            $user->save();
            return View::make('profile.index', compact('user'));
        }

        // update user password
        else if ($type == "password") {
            $oldPass = Input::get('old_password');
            $password = Input::get('password');
            $passwordConf = Input::get('password_confirmation');

            if ($password == $passwordConf && Hash::check($oldPass, $request->user()->password)) {
                $user->update(['password' => bcrypt($password)]);
                return view('profile.edit', compact('user'));
            } else if ($password != $passwordConf) {
                echo 'password & password confirmation do not match';
            } else {
                echo 'your current password does not match our records';
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
