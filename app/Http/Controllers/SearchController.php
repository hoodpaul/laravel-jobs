<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Search;
use Input;

class SearchController extends Controller
{

    private function formatArgs(array $args)
    {
        $format = "";
        for ($i = 0; $i < count($args); $i++) {
            $format .= sprintf("\"%s\"%s", $args[$i],
                $i == count($args) - 1 ? '' : ' ');
        }
        return $format;
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getJobs(Request $request)
    {
/*        $jobs = file_get_contents("http://api.indeed.com/ads/apisearch?publisher=4188194085276129&q=java&l=austin%2C+tx&sort=&radius=&st=&jt=&start=&limit=&fromage=&filter=&latlong=1&co=us&chnl=&userip=1.2.3.4&useragent=Mozilla/%2F4.0%28Firefox%29&v=2&format=json");
        dd(json_decode($jobs));*/
        // return just the search form if no posts are submitted
        if (!empty(Input::all())) {

            // call java program to scrape jobs from indeed with formatted args
            $data = exec("cd WebScraper && java -cp .:./lib/*:bin com.hoodp.Main " .
                $this->formatArgs([Input::get("Job_Title"), Input::get("Location")]));

            // format the results
            $data = json_decode($data);
            return view('search.job', compact('data'));
        } else {
            return view('search.job');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getCompanies(Request $request)
    {
        if (!empty(Input::all())) {
            $data = $this->getData($request);
            return view('search.company', compact('data'));
        } else {
            return view('search.company');
        }
    }

    public function getData(Request $request)
    {
        // get required parameters for search data
        $ip = $request->ip();
        $user_agent = $request->server('HTTP_USER_AGENT');
        $requests = Input::all();

        $glassdoor = new \App\Library\Api(
            ['t.p' => '47506'],
            ['t.k' => 'erRavMJczgE'],
            'http://api.glassdoor.com/api/api.htm'
        );
        dd($glassdoor);

        // return the data
        return Search::getData($ip, $user_agent, $requests);

    }
}
