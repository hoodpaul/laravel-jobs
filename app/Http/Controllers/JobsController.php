<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Job;
use App\JobFile;
use App\File as FileEntry;
use DB;
use App\FileType;
use App\JobStatus;
use Illuminate\Support\Facades\Input;
use Redirect;
use Response;

class JobsController extends Controller
{
    /** validation rules */
    protected $rules = array(
        'title' => 'required|max:255',
        'company' => 'required|max:255',
        'location' => 'required|max:255'
    );

    public function create(Request $request)
    {

        // Get all files by type.
        $types = $this->filesByType($request);
        return view('jobs.create', compact('types'));
    }


    public function edit(Request $request, Job $job)
    {
        $types = $this->filesByType($request);
        return view('jobs.edit', compact('job', 'types'));
    }

    private function filesByType(Request $request)
    {

        // Get all of the user's files by file type.
        return FileType::with(['files' => function($query) use ($request) {
            $query->where('user_id', Input::user()->id);
        }])->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Get all of the user's files by file type.
        $types = $this->filesByType($request);

        // Get all of the job statuses.
        $status = JobStatus::all();
        return view('jobs.index', compact('status', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        // Validate the form data.
        $this->validate($request, $this->rules);

        // Create new job model.
        $job = new Job(Input::except('files'));

        // Save the job by using the user model.
        Input::user()->jobs()->save($job);

        // Save the associated files.
        if (Input::exists('files')) {

            // Attach the associated files to the job model.
            $job->files()->sync(Input::get('files'));
        }
        return Redirect::to('./jobs');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $jobs = Job::where('user_id', Input::user()->id)
            ->with('status', 'files.image')
            ->orderBy('updated_at', 'desc')
            ->get();
        return Response::json($jobs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Job $job)
    {

        // Validate the form data unless the status id is set.
        if (!(Input::get('status_id')))
            $this->validate($request, $this->rules);

        // Update the job with new data.
        $job->update(Input::except('files'));

        // Check if the files should be updated.
        if (Input::exists('files')) {

            // Attach the associated files to the job model.
            $job->files()->sync(Input::get('files'));
        }
        return Input::get('status_id') ? Response::json($job->id) : Redirect::to('jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Job $job)
    {
        return Response::json($job, $job->delete() ? 200 : 500);
    }
}
