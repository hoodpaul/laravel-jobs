<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes.
Route::get('login', 'Auth\AuthController@getLogin');
Route::get('logout', 'Auth\AuthController@getLogout')->name("logout");
Route::post('login', 'Auth\AuthController@postLogin')->name("login");

// Registration routes.
Route::get('register', 'Auth\AuthController@getRegister')->name("register");
Route::post('register', 'Auth\AuthController@postRegister');

Route::get(' ', function() {
    return Redirect::to('login');
});

// Password reset link request routes...
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\PasswordController@postEmail'
]);

// Password reset routes...
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\PasswordController@getReset'
]);
Route::post('password/reset', [
    'as' => 'password.reset',
    'uses' => 'Auth\PasswordController@postReset'
]);

Route::get('/', function() {
    Redirect::to('login');
});

Route::group(['middleware' => 'auth'], function() {

    // Bind jobs.
    Route::bind('jobs', function($value, $route) {
        return App\Job::find($value);
    });

    // Bind user information to profile view.
    Route::bind('profile', function($value, $route) {
        return App\User::find($value);
    });

    // Resource for dashboard homepage.
    Route::resource('dash', 'DashboardController', [
        'only' => ['index'],
    ]);

    // Resource for the email page.
    Route::resource('email', 'EmailController', [
        'only' => ['index']
    ]);

    // Handles the profile data.
    Route::resource('profile', 'ProfileController');

    // Handles specific job information.
    Route::resource('jobs', 'JobsController');

    // Handle get request while searching for jobs.
    Route::get('search/jobs', [
        'as' => 'search.jobs',
        'uses' => 'SearchController@getJobs'
    ]);

    // Handle get request to search for companies.
    Route::get("search/companies", [
        "as" => "search.companies",
        "uses" => "SearchController@getCompanies"
    ]);

    // Route for handling avatar image retrieval.
    Route::get("images/profile/{id}", function($id) {
        $filePath = sprintf("%s/%s", storage_path('uploads'),
            App\User::find($id)->image->filename);
        return Response::download($filePath);
    });

    Route::resource("files", "FileController", ["only" => [
        "index", "store", "destroy", "update"
    ]]);

    Route::get("files/{id}/{name}", [
        "as" => "files.show",
        "uses" => "FileController@show"
    ]);

    // Route for showing all of the user's files.
    Route::get("files/all", [
        'as' => 'files.all',
        'uses' => 'FileController@showAll'
    ]);

    // All of the calls below use ajax.
    Route::post("/search/jobs/addJob", "JobsController@store");
    Route::post("/jobs/updateAppStatus", "JobsController@updateAppStatus");
});