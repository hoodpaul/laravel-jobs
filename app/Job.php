<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

    /** @var array prevents mass assignment exception  */
    protected $guarded = [];

    /** @var array fields that are displayed on the table */
    protected $outFields = array(
        'title', 'company', 'location', 'applied', 'actions'
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }

    public function files()
    {
        return $this->belongsToMany('App\File', 'job_files');
    }

    public function status()
    {
        return $this->hasOne('App\JobStatus', 'id', 'status_id');
    }
}
