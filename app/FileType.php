<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FileType
 *
 * @mixin \Eloquent
 */
class FileType extends Model
{
    protected $guarded = [];
    protected $table = "file_types";
    public $timestamps = false;

    public function scopeTest($query, $name)
    {
        return $query->where('name', $name);
    }

    public function noActiveStatus()
    {
        return (int) $this->id == config('constants.OTHER_FILE_TYPE');
    }

    public function files()
    {
        return $this->hasMany('App\File', 'file_type');
    }
}
