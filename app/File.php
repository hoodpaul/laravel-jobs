<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    const AVATAR_PATH = 'avatars';

    protected $guarded = [];
    protected $table = "file_entries";
    public $timestamps = false;
    protected $fillable = [
        "mime", "original_filename", "filename", "file_type", "user_id",
        "size", "file_image", "active"
    ];

    public function type()
    {
        return preg_replace('/\w+\//', '', $this->mime);
    }

    /**
     * @return int|mixed
     */
/*    public function getSizeAttribute($value)
    {

        // File suffixes.
        $sizes = [
            "B", "KB", "MB", "GB", "TB"
        ];

        // Loop until the size is less than 1000.
        while (count($sizes) > 1 && $value > 1000) {
            $value /= 1024;
            array_shift($sizes);
        }
        return sprintf("%s %s", ceil($value), $sizes[0]);
    }*/

    public function formatTime($format = "M j, Y g:i A")
    {
        return date($format, strtotime($this->created_at));
    }

    public function getCreatedAtAttribute($value)
    {
        return date('M j, Y g:i A', strtotime($value));
    }

    public function image()
    {
        return $this->hasOne('App\FileImage', 'id', 'file_image');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function kind()
    {
        return $this->hasOne('App\FileType', 'id', 'file_type');
    }

    public function jobs()
    {
        return $this->belongsToMany('App\Job', 'job_files');
    }
}
