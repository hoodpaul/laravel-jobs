<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FileImage
 *
 * @mixin \Eloquent
 */
class FileImage extends Model
{
    protected $guarded = [];
    protected $table = "file_images";

    /** Image file path. */
    public function path()
    {
        return "img/file-search/" . $this->name . ".png";
    }


}
