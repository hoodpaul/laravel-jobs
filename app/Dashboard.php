<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
use Job;

class Dashboard extends Model
{
    protected $table = "users";

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function jobs()
    {
        return $this->hasMany('App\Job');
    }
}
