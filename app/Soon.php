<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soon extends Model
{
    protected $guarded = [];
    protected $table = "future_users";
    public $timestamps = false;
}
