<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobFile extends Model
{
    protected $table = "job_files";
    protected $primaryKey = ['job_id', 'file_id'];
    public $timestamps = false;

    protected $fillable = ['job_id', 'file_id'];
    
    public function file()
    {
        return $this->belongsTo('App\File', 'file_id', 'id');
    }

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id');
    }
}
