<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobStatus extends Model
{
    protected $table = "job_status";
    public $timestamps = false;

    public function jobs()
    {
        return $this->hasMany('App\Job', 'id', 'status_id');
    }
}
